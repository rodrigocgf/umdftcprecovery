#pragma once


#include "stdafx.h"
#include <vector>

namespace UMDF
{
	class CConfig
	{
	public:
		CConfig(void);
		~CConfig(void);
		CConfig(const CConfig& other);
		CConfig& operator=(const CConfig& other);
		void ParseChannels(const string & channels);


		int				iMaxChunkSize;
		bool			bPersistMessages;
		std::string		m_strChannel;
		int				m_iCircularBufferSize;
		int				m_iMaximumQueryableSize;
		int				m_iMaxRawDataLength;
		std::string		m_strMessagesFilePath;
		std::list<string>		m_listChannels;
	};

}