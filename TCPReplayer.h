#pragma once

#include "stdafx.h"

#include <deque>
#include <vector>
#include <algorithm>

#include "quickfix/fix44/message.h"
#include "quickfix/fix44/ApplicationMessageRequest.h"
#include "quickfix/fix44/ApplicationMessageRequestAck.h"
#include "quickfix/fix44/ApplicationRawDataReporting.h"
#include "quickfix/fix44/ApplicationMessageReport.h"
#include "DecNumber.hpp"
#include "ChannelManager.h"


namespace UMDF
{	

	class CTCPReplayer :	public BTS::CGatewayPluginImpl, 
							public UMDF::IEncodedMessageStorage,
							public ITCPReplayer
							
	{
	public:
		CTCPReplayer(LPCTSTR szPluginID);
		~CTCPReplayer(void);

		bool OnLoad();
		bool OnStart();
		void OnStop();
		void OnLogon(const FIX::SessionID &session);
		void OnLogout(const FIX::SessionID &session);
		UINT OnMessage(const FIX::Message &message, const BTS::Sender &sender);
		LPCTSTR GetPluginID() const {return m_strPluginID.c_str();}
		bool store(const std::string& strChannel, FIX::SEQNUM msgSeqNum, const std::string& buffer);
		bool resetSeqNum(const std::string& strChannel);

		///<summary>
		/// Interface ITCPReplayer
		///</summary>
		void reply(FIX44::Message &message, const FIX::SessionID & session);
		std::string getNextRptID(const FIX::SessionID &session);		
		std::string getNextRespID(const FIX::SessionID &session);		
		
		bool IsSessionOngoing(const FIX::SessionID & sessionID);
		void SetSessionOngoing( const FIX::SessionID & sessionID, bool value );
	private:
		std::string genUID(const FIX::SessionID &session, const char ch  );

		const std::string					m_strPluginID;
		boost::shared_ptr< boost::thread >	m_ApplMsgReqThreadPtr;
		boost::shared_ptr< boost::thread >	m_WriterThreadPtr;
		CConfig								m_config;
		long								m_uiIDGenSeqNum;
		
		QueueManager<CProcessApplMsgRequest>	m_ApplMsgReqQMngr;
		QueueManager<CWriter>					m_WriterQMngr;
		
		boost::shared_ptr<ChannelManager>		m_channelManagerPtr;

		boost::mutex							m_SessionMutex;
		std::map< std::string , bool >			m_MapSessionOngoing;		

	};

	typedef std::map< std::string , bool >::iterator	ItMapSessionOngoing;

}

DECLARE_FREEINSTANCE_PLUGIN_FACTORY(UMDF::CTCPReplayer)