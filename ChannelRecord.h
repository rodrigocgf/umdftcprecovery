#pragma once

#include "stdafx.h"
	
namespace UMDF
{

	class ChannelRecord
	{
	public:
		ChannelRecord(__int64 iNum, const std::string &strData);
		~ChannelRecord(void);

		__int64 getNum() const;
		__int64 getLength() const;
		const char * getData() const;
		bool operator<(const ChannelRecord & r) const; 
		operator __int64 () const;

		ChannelRecord(const ChannelRecord& other)
		{
			m_iNum = other.m_iNum;
			m_iLength = other.m_iLength;
			m_spbData = other.m_spbData;
		}

		ChannelRecord& operator=(const ChannelRecord& other)
		{
			m_iNum = other.m_iNum;
			m_iLength = other.m_iLength;
			m_spbData = other.m_spbData;
			return *this;
		}

		int getToken() const { return m_iToken; }
		void setToken(int tkn) 
		{ 
			ASSERT( m_iToken < 0 );
			ASSERT( tkn > 0 );
			m_iToken = tkn; 
		}
		void free() 
		{
			m_spbData.reset();
			m_iLength = 0;
		}
	private:
		boost::shared_array<const char> m_spbData;
		__int64 m_iLength;
		__int64 m_iNum;
		int m_iToken;
	};

}