#pragma once

#include "stdafx.h"

#include <iomanip>
#include <fstream>
#include <iostream>
#include <io.h>
#include <errno.h>
#include <share.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <boost/unordered_map.hpp>

typedef boost::unordered_map<__int64, __int64> MapI64;

class IndexManager
{
	public:
		IndexManager(void);
		~IndexManager(void);

		void addMessage(__int64 offset, __int64 size, __int64 messageseq);
		BOOL removeMessage(__int64 messageseq);
		BOOL findMessage(__int64 messageseq, FileIndex &fileindex);
		size_t getIndexCount();
		const FileIndex& getIndex(DWORD index);

	private:
		MapI64			m_mapMsgSeqToPos;			// Map de CtrlNumbers (Megabolsa) para posi��o dentro do vetor de indices
		FileIndexVector m_vectMsgIndex;			// Vetor com posi��o e tamanho dos registros

		__int64 appendIndexMessage(__int64 offset, __int64 size);		
		__int64 appendIndexMessage(const FileIndex &fileindex);
		void  setIndexMessage(__int64 index, __int64 offset,  __int64 size);
		void  setIndexMessage(__int64 index, const FileIndex &fileindex);
};