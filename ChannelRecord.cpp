#include "StdAfx.h"
#include "ChannelRecord.h"

namespace UMDF
{

	ChannelRecord::ChannelRecord(__int64 iNum, const std::string &strData) :
		  m_iNum(iNum),
		  m_iLength(strData.length()),
		  m_iToken(-1)
	{
		m_spbData.reset(new char[m_iLength]);
		memcpy((void*)m_spbData.get() , strData.data() , m_iLength );
	}

	ChannelRecord::~ChannelRecord(void)
	{
	}	

	__int64 ChannelRecord::getNum() const 
	{ 
		return m_iNum; 
	}

	__int64 ChannelRecord::getLength() const
	{
		return m_iLength;
	}

	const char * ChannelRecord::getData() const
	{
		return m_spbData.get();
	}

	bool ChannelRecord::operator<(const ChannelRecord & r) const 
	{ 
		return m_iNum < r.m_iNum; 
	}

	ChannelRecord::operator __int64 () const 
	{ 
		return m_iNum; 
	}

}