#pragma once

extern "C" {
#include <decQuad.h>
};
#include <string>

namespace UMDFUtil 
{
using std::string;
class DecContext : public decContext
{
public:
    /// <summary>Constructor</summary>
    DecContext ();
    /// <summary>This function is used to clear (set to zero) one or more status bits in the status field of a decContext.</summary>
    /// <param name='status'>Any 1 (set) bit in this argument will cause the corresponding bit to be cleared in the context status field.</param>
    /// <returns>this</returns>
    DecContext& clearStatus (uint32_t status);
    /// <summary>This function is used to initialize a decContext structure to default values. 
    /// It is strongly recommended that this function always be used to initialize a decContext structure, 
    /// even if most or all of the fields are to be set explicitly 
    /// (in case new fields are added to a later version of the structure).</summary>
    /// <param name='kind'><p>The kind of initialization to be performed. Only the values defined
    /// in the decContext header file are permitted (any other value will initialize the
    /// structure to a valid condition, but with the DEC_Invalid_operation status bit
    /// set).</p>
    /// <p>When kind is DEC_INIT_BASE, the defaults for the ANSI X3.274 arithmetic
    /// subset are set. That is, the digits field is set to 9, the emax field is set to
    /// 999999999, the round field is set to ROUND_HALF_UP, the status field is cleared
    /// (all bits zero), the traps field has all the DEC_Errors bits set (DEC_Rounded,
    /// DEC_Inexact, DEC_Lost_digits, and DEC_Subnormal are 0), clamp is set to 0,
    /// and extended (if present) is set to 0.</p>
    /// <p>When kind is DEC_INIT_DECIMAL32 or DEC_INIT_DECSINGLE, defaults for a
    /// decimal32 number using IEEE 754 rules are set. That is, the digits field is set
    /// to 7, the emax field is set to 96, the emin field is set to �95, the round field is
    /// set to DEC_ROUND_HALF_EVEN, the status field is cleared (all bits zero), the traps
    /// field is cleared (no traps are enabled), clamp is set to 1, and extended (if present)
    /// is set to 1.</p>
    /// <p>When kind is DEC_INIT_DECIMAL64 or DEC_INIT_DECDOUBLE, defaults for a
    /// decimal64 number using IEEE 754 rules are set. That is, the digits field is set
    /// to 16, the emax field is set to 384, the emin field is set to �383, and the other
    /// fields are set as for DEC_INIT_DECIMAL32.</p>
    /// <p>When kind is DEC_INIT_DECIMAL128 or DEC_INIT_DECQUAD, defaults for a
    /// decimal128 number using IEEE 754 rules are set. That is, the digits field is set
    /// to 34, the emax field is set to 6144, the emin field is set to �6143, and the other
    /// fields are set as for DEC_INIT_DECIMAL32.</p></param>
    /// <returns>this</returns>
    DecContext& setDefault (int32_t kind);
    /// <summary>This function is used to return the round (rounding mode) field of a decContext.</summary>
    /// <returns>the enum rounding rounding mode.</returns>
    enum rounding getRounding ();
    /// <summary>This function is used to return the status field of a decContext.</summary>
    /// <returns>Returns the uint32_t status field.</returns>
    uint32_t getStatus();
    /// <summary>This function is used to restore one or more status bits in the status field of a decContext
    /// from a saved status field.</summary>
    /// <param name='status'>A saved status field (as saved by decContextSaveStatus or
    /// retrieved by decContextGetStatus).</param>
    /// <param name='mask'>Any 1 (set) bit in this argument will cause the corresponding bit
    /// to be restored (set to 0 or 1, taken from the corresponding bit in status) in the
    /// context status field.</param>
    /// <returns>this</returns>
    DecContext& restoreStatus(uint32_t status, uint32_t mask);
    /// <summary>This function is used to save one or more status bits from the status field of a decContext.</summary>
    /// <param name='mask'>Any 1 (set) bit in this argument will cause the corresponding bit
    /// to be saved from the context status field.</param>
    /// <returns>the uint32_t which is the logical And of the context status field and the mask.</returns>
    uint32_t saveStatus(uint32_t mask);
    /// <summary>This function is used to set the rounding mode in the round field of a decContext.</summary>
    /// <param name='rounding'>The rounding mode to be copied to the context round field.</param>
    /// <returns>this</returns>
    DecContext& setRounding (enum rounding rounding);
    /// <summary>This function is used to set one or more status bits in the status field of a decContext. If
    /// any of the bits being set have the corresponding bit set in the traps field, a trap is raised
    /// (regardless of whether the bit is already set in the status field). Only one trap is raised
    /// even if more than one bit is being set.</summary>
    /// <param name='status'>Any 1 (set) bit in this argument will cause the corresponding bit
    /// to be set in the context status field.</param>
    /// <returns>this</returns>
    DecContext& setStatus (uint32_t status);
    /// <summary>This function is used to set a status bit in the status field of a decContext, using the name
    /// of the bit as returned by the decContextStatusToString function. If the bit being set has
    /// the corresponding bit set in the traps field, a trap is raised (regardless of whether the bit
    /// is already set in the status field).</summary>
    /// <param name='str'>A string which must be exactly equal to one that might be returned
    /// by decContextStatusToString. If the string is �No status�, the status is not
    /// changed and no trap is raised. If the string is �Multiple status�, or is not
    /// recognized, then the call is in error.</param>
    /// <returns>this</returns>
    DecContext& setStatusFromString (const string& str);
    /// <summary>This function is identical to decContextSetStatusFromString except that the context traps
    /// field is ignored (i.e., no trap is raised).</summary>
    /// <param name='str'>A string which must be exactly equal to one that might be returned
    /// by decContextStatusToString. If the string is �No status�, the status is not
    /// changed and no trap is raised. If the string is �Multiple status�, or is not
    /// recognized, then the call is in error.</param>
    /// <returns>this</returns>
    DecContext& setStatusFromStringQuiet (const string& str);
    /// <summary>This function is identical to decContextSetStatus except that the context traps field is
    /// ignored (i.e., no trap is raised).</summary>
    /// <param name='status'>Any 1 (set) bit in this argument will cause the corresponding bit
    /// to be set in the context status field.</param>
    /// <returns>this</returns>
    DecContext& setStatusQuiet (uint32_t status);
    /// <summary>This function returns a pointer (char *) to a human-readable description of a status bit.
    /// The string pointed to will be a constant.</summary>
    /// <returns>If no bits are set in the status field, a pointer to the string �No status� is returned. If
    /// more than one bit is set, a pointer to the string �Multiple status� is returned.</returns>
    string statusToString ();
    /// <summary>This function checks that the DECLITEND tuning parameter (see page 67) is set correctly.</summary>
    /// <param name='quiet'>If 0, a warning message is displayed (using printf) if DECLITEND is
    /// set incorrectly. If 1, no message is displayed.</param>
    /// <returns>Returns 0 if the DECLITEND parameter is correct, 1 if it is incorrect and should be set to
    /// 1, and -1 if it is incorrect and should be set to 0.</returns>
    static int32_t testEndian (uint8_t quiet);
    /// <summary>This function is used to test one or more status bits in a saved status field.</summary>
    /// <param name='status'>A saved status field (as saved by decContextSaveStatus or
    /// retrieved by decContextGetStatus).</param>
    /// <param name='mask'>Any 1 (set) bit in this argument will cause the corresponding bit
    /// in status to be included in the test.</param>
    /// <returns>Returns the uint32_t which is the logical And of status and mask.</returns>
    static uint32_t testSavedStatus (uint32_t status, uint32_t mask);
    /// <summary>This function is used to test one or more status bits in a context.</summary>
    /// <param name='mask'>Any 1 (set) bit in this argument will cause the corresponding bit
    /// in context status field to be included in the test.</param>
    /// <returns>Returns the uint32_t which is the logical And of the context status field and mask.</returns>
    uint32_t testStatus (uint32_t mask);
    /// <summary>This function is used to clear (set to zero) all the status bits in the status field of a
    /// decContext.</summary>
    /// <returns>this</returns>
    DecContext& zeroStatus ();
};
/// <summary>
/// Encapsulation of the "decNumber" struct (from 
/// </summary>
class DecNumber
{
private:
    decQuad value_;
public:
    /// <summary>Constructor</summary>
    DecNumber();
    /// <summary>Copy Constructor</summary>
    DecNumber(const DecNumber&);
    /// <summary>Assignment Operator</summary>
    DecNumber& operator = (const DecNumber&);
    /// <summary>Constructor, </summary>
    /// <param name='i'>Value to be converted</param>
    DecNumber (int32_t i);
    /// <summary></summary>
    /// <param name='u'>Value to be converted</param>
    explicit DecNumber (uint32_t u);
    /// <summary></summary>
    /// <param name=''></param>
    DecNumber (const string& str);
    /// <summary></summary>
    /// <param name=''></param>
    /// <param name=''></param>
    DecNumber (const string& str, DecContext& context);
    /// <summary></summary>
    operator int();
    /// <summary></summary>
//    string toString();
    /// <summary></summary>
    string toStringFixed() const;
    /// <summary></summary>
    string toEngString() const;
    /// <summary></summary>
    /// <param name=''></param>
    uint32_t toUInt32(DecContext& context, enum rounding rounding);
    /// <summary></summary>
    /// <param name=''></param>
    int32_t toInt32(DecContext& context, enum rounding rounding);
    /* Operators and elementary functions                               */
    /// <summary></summary>
    /// <param name=''></param>
    DecNumber abs (DecContext& context) const;
    /// <summary></summary>
    /// <param name=''></param>
    /// <param name=''></param>
    static DecNumber abs (const DecNumber& number, DecContext& context);
    //decNumber * decNumberAdd(decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    /// <param name=''></param>
    DecNumber add (const DecNumber& rhs, DecContext& context) const;
    /// <summary></summary>
    /// <param name=''></param>
    /// <param name=''></param>
    /// <param name=''></param>
    static DecNumber add (const DecNumber& lhs, const DecNumber& rhs, DecContext& context);
    //decNumber * decNumberAnd(decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    /// <param name='rhs'>Second argument (first is 'this')</param>
    /// <param name='context'>A DecContext</param>
    DecNumber and (const DecNumber& rhs, DecContext& context) const;
    /// <summary></summary>
    static DecNumber and (const DecNumber& lhs, const DecNumber& rhs, DecContext& context);
    //decNumber * decNumberCompare(decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    /// <param name='rhs'>Second argument (first is 'this')</param>
    /// <param name='context'>A DecContext</param>
    DecNumber compare (const DecNumber& rhs, DecContext& context) const;
    /// <summary></summary>
    static DecNumber compare (const DecNumber& lhs, const DecNumber& rhs, DecContext& context);
    /// <summary></summary>
    /// <param name='rhs'>Second argument (first is 'this')</param>
    /// <param name='context'>A DecContext</param>
    int intCompare (const DecNumber& rhs, DecContext& context) const;
    /// <summary></summary>
    static int intCompare (const DecNumber& lhs, const DecNumber& rhs, DecContext& context);
    //decNumber * decNumberCompareSignal(decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    /// <param name='rhs'>Second argument (first is 'this')</param>
    /// <param name='context'>A DecContext</param>
    DecNumber compareSignal (const DecNumber& rhs, DecContext& context) const;
    /// <summary></summary>
    static DecNumber compareSignal (const DecNumber& lhs, const DecNumber& rhs, DecContext& context);
    //decNumber * decNumberCompareTotal(decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    /// <param name='rhs'>Second argument (first is 'this')</param>
    /// <param name='context'>A DecContext</param>
    DecNumber compareTotal (const DecNumber& rhs) const;
    /// <summary></summary>
    static DecNumber compareTotal (const DecNumber& lhs, const DecNumber& rhs);
    //decNumber * decNumberCompareTotalMag(decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    /// <param name='rhs'>Second argument (first is 'this')</param>
    /// <param name='context'>A DecContext</param>
    DecNumber compareTotalMag (const DecNumber& rhs) const;
    /// <summary></summary>
    static DecNumber compareTotalMag (const DecNumber& lhs, const DecNumber& rhs);
    //decNumber * decNumberDivide(decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    /// <param name='rhs'>Second argument (first is 'this')</param>
    /// <param name='context'>A DecContext</param>
    DecNumber divide (const DecNumber& rhs, DecContext& context) const;
    /// <summary></summary>
    /*static*/DecNumber divide (const DecNumber& lhs, const DecNumber& rhs, DecContext& context);
    //decNumber * decNumberDivideInteger(decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber divideInteger (const DecNumber& rhs, DecContext& context) const;
    /// <summary></summary>
    static DecNumber divideInteger (const DecNumber& lhs, const DecNumber& rhs, DecContext& context);
    //decNumber * decNumberExp(decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber exp (DecContext& context) const;
    /// <summary></summary>
    static DecNumber exp (const DecNumber& number, DecContext& context);
    //decNumber * decNumberFMA(decNumber *, const decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber FMA (const DecNumber& rhs, const DecNumber& fhs, DecContext& context) const;
    /// <summary></summary>
    static DecNumber FMA (const DecNumber& lhs, const DecNumber& rhs, const DecNumber& fhs, DecContext& context);
    //decNumber * decNumberInvert(decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber invert (DecContext& context) const;
    /// <summary></summary>
    static DecNumber invert (const DecNumber& number, DecContext& context);
    //decNumber * decNumberLn(decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber ln (DecContext& context) const;
    /// <summary></summary>
    static DecNumber ln (const DecNumber& number, DecContext& context);
    //decNumber * decNumberLogB(decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber logB (DecContext& context) const;
    /// <summary></summary>
    static DecNumber logB (const DecNumber& number, DecContext& context);
    //decNumber * decNumberLog10(decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber log10 (DecContext& context) const;
    /// <summary></summary>
    static DecNumber log10 (const DecNumber& number, DecContext& context);
    //decNumber * decNumberMax(decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber max_ (const DecNumber& rhs, DecContext& context) const;
    /// <summary></summary>
    static DecNumber max_ (const DecNumber& lhs, const DecNumber& rhs, DecContext& context);
    //decNumber * decNumberMaxMag(decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber maxMag (const DecNumber& rhs, DecContext& context) const;
    /// <summary></summary>
    static DecNumber maxMag (const DecNumber& lhs, const DecNumber& rhs, DecContext& context);
    //decNumber * decNumberMin(decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber min_ (const DecNumber& rhs, DecContext& context) const;
    /// <summary></summary>
    static DecNumber min_ (const DecNumber& lhs, const DecNumber& rhs, DecContext& context);
    //decNumber * decNumberMinMag(decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber minMag (const DecNumber& rhs, DecContext& context) const;
    /// <summary></summary>
    static DecNumber minMag (const DecNumber& lhs, const DecNumber& rhs, DecContext& context);
    //decNumber * decNumberMinus(decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber minus (DecContext& context) const;
    /// <summary></summary>
    static DecNumber minus (const DecNumber& number, DecContext& context);
    //decNumber * decNumberMultiply(decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber multiply (const DecNumber& rhs, DecContext& context) const;
    /// <summary></summary>
    static DecNumber multiply (const DecNumber& lhs, const DecNumber& rhs, DecContext& context);
    //decNumber * decNumberOr(decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber or (const DecNumber& rhs, DecContext& context) const;
    /// <summary></summary>
    static DecNumber or (const DecNumber& lhs, const DecNumber& rhs, DecContext& context);
    //decNumber * decNumberPlus(decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber plus (DecContext& context) const;
    /// <summary></summary>
    static DecNumber plus (const DecNumber& number, DecContext& context);
    //decNumber * decNumberPower(decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber power (const DecNumber& rhs, DecContext& context) const;
    /// <summary></summary>
    static DecNumber power (const DecNumber& lhs, const DecNumber& rhs, DecContext& context);
    //decNumber * decNumberQuantize(decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber quantize (const DecNumber& rhs, DecContext& context) const;
    /// <summary></summary>
    static DecNumber quantize (const DecNumber& lhs, const DecNumber& rhs, DecContext& context);
    //decNumber * decNumberReduce(decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber reduce (DecContext& context) const;
    /// <summary></summary>
    static DecNumber reduce (const DecNumber& number, DecContext& context);
    //decNumber * decNumberRemainder(decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber remainder (const DecNumber& rhs, DecContext& context) const;
    /// <summary></summary>
    static DecNumber remainder (const DecNumber& lhs, const DecNumber& rhs, DecContext& context);
    //decNumber * decNumberRemainderNear(decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber remainderNear (const DecNumber& rhs, DecContext& context) const;
    /// <summary></summary>
    static DecNumber remainderNear (const DecNumber& lhs, const DecNumber& rhs, DecContext& context);
    //decNumber * decNumberRotate(decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber rotate (const DecNumber& rhs, DecContext& context) const;
    /// <summary></summary>
    static DecNumber rotate (const DecNumber& lhs, const DecNumber& rhs, DecContext& context);
    //decNumber * decNumberSameQuantum(decNumber *, const decNumber *, const decNumber *);
    /// <summary></summary>
    bool sameQuantum (const DecNumber& rhs) const;
    /// <summary></summary>
    static bool sameQuantum (const DecNumber& lhs, const DecNumber& rhs);
    //decNumber * decNumberScaleB(decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber scaleB (const DecNumber& rhs, DecContext& context) const;
    /// <summary></summary>
    static DecNumber scaleB (const DecNumber& lhs, const DecNumber& rhs, DecContext& context);
    //decNumber * decNumberShift(decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber shift (const DecNumber& rhs, DecContext& context) const;
    /// <summary></summary>
    static DecNumber shift (const DecNumber& lhs, const DecNumber& rhs, DecContext& context);
    //decNumber * decNumberSquareRoot(decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber squareRoot (DecContext& context) const;
    /// <summary></summary>
    static DecNumber squareRoot (const DecNumber& number, DecContext& context);
    //decNumber * decNumberSubtract(decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber subtract (const DecNumber& rhs, DecContext& context) const;
    /// <summary></summary>
    static DecNumber subtract (const DecNumber& lhs, const DecNumber& rhs, DecContext& context);
    //decNumber * decNumberToIntegralExact(decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber toIntegralExact (DecContext& context) const;
    /// <summary></summary>
    static DecNumber toIntegralExact (const DecNumber& number, DecContext& context);
    //decNumber * decNumberToIntegralValue(decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber toIntegralValue (DecContext& context, enum rounding rounding) const;
    /// <summary></summary>
    static DecNumber toIntegralValue (const DecNumber& number, DecContext& context, enum rounding rounding);
    //decNumber * decNumberXor(decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber xor (const DecNumber& rhs, DecContext& context) const;
    /// <summary></summary>
    static DecNumber xor (const DecNumber& lhs, const DecNumber& rhs, DecContext& context);

    ///* Utilities                                                        */
    //enum decClass decNumberClass(const decNumber *, decContext *);
    /// <summary></summary>
    enum decClass getClass ();
    //decNumber  * decNumberCopy(decNumber *, const decNumber *);
    /// <summary></summary>
    static DecNumber copy (const DecNumber& number);
    //decNumber  * decNumberCopyAbs(decNumber *, const decNumber *);
    /// <summary></summary>
    static DecNumber copyAbs (const DecNumber& number);
    //decNumber  * decNumberCopyNegate(decNumber *, const decNumber *);
    /// <summary></summary>
    static DecNumber copyNegate (const DecNumber& number);
    //decNumber  * decNumberCopySign(decNumber *, const decNumber *, const decNumber *);
    /// <summary></summary>
    static DecNumber copySign (const DecNumber& source, const DecNumber& pattern);
    /// <summary></summary>
    DecNumber copySign (const DecNumber& pattern) const;
    //decNumber  * decNumberNextMinus(decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber nextMinus (DecContext& context) const;
    /// <summary></summary>
    static DecNumber nextMinus (const DecNumber& number, DecContext& context);
    //decNumber  * decNumberNextPlus(decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber nextPlus (DecContext& context) const;
    /// <summary></summary>
    static DecNumber nextPlus (const DecNumber& number, DecContext& context);
    //decNumber  * decNumberNextToward(decNumber *, const decNumber *, const decNumber *, decContext *);
    /// <summary></summary>
    DecNumber nextToward (const DecNumber& y, DecContext& context) const;
    /// <summary></summary>
    static DecNumber nextToward (const DecNumber& x, const DecNumber& y, DecContext& context);
    //const char * decNumberVersion(void);
    /// <summary></summary>
    static string version ();
    //decNumber  * decNumberZero(decNumber *);
    /// <summary></summary>
    DecNumber& zero();

    ///* Functions for testing decNumbers (normality depends on context)  */
    //int32_t decNumberIsNormal(const decNumber *, decContext *);
    /// <summary></summary>
    int32_t isNormal() const;
    /// <summary></summary>
    static int32_t isNormal(DecNumber& number);
    //int32_t decNumberIsSubnormal(const decNumber *, decContext *);
    /// <summary></summary>
    int32_t isSubnormal() const;
    /// <summary></summary>
    static int32_t isSubnormal(DecNumber& number);

    ///* Macros for testing decNumber *dn                                 */
    //#define decNumberIsCanonical(dn) (1)  /* All decNumbers are saintly */
    /// <summary></summary>
    bool isCanonical() const;
    /// <summary></summary>
    static bool isCanonical(DecNumber& number);
    //#define decNumberIsFinite(dn)    (((dn)->bits&DECSPECIAL)==0)
    /// <summary></summary>
    bool isFinite() const;
    /// <summary></summary>
    static bool isFinite(DecNumber& number);
    //#define decNumberIsInfinite(dn)  (((dn)->bits&DECINF)!=0)
    /// <summary></summary>
    bool isInfinite() const;
    /// <summary></summary>
    static bool isInfinite(DecNumber& number);
    //#define decNumberIsNaN(dn)       (((dn)->bits&(DECNAN|DECSNAN))!=0)
    /// <summary></summary>
    bool isNaN() const;
    /// <summary></summary>
    static bool isNaN(DecNumber& number);
    //#define decNumberIsNegative(dn)  (((dn)->bits&DECNEG)!=0)
    /// <summary></summary>
    bool isNegative() const;
    /// <summary></summary>
    static bool isNegative(DecNumber& number);
    /// <summary></summary>
    bool isSpecial() const;
    /// <summary></summary>
    static bool isSpecial(DecNumber& number);
    //#define decNumberIsZero(dn)      (*(dn)->lsu==0 \
    //                                  && (dn)->digits==1 \
    //                                  && (((dn)->bits&DECSPECIAL)==0))
    /// <summary></summary>
    bool isZero() const;
    /// <summary></summary>
    static bool isZero(DecNumber& number);
    //#define decNumberRadix(dn)       (10)
    /// <summary></summary>
    static int radix();
};

};