#include "stdafx.h"
#include "Util.h"

#pragma warning (push)
#pragma warning (disable: 4996)
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#pragma warning (pop)

namespace Util {

using std::string;
using std::set;

/// <summary>Converts a string formatted as 'yyyyMMddHHmmss' or 'yyyyMMddHHmmssSSS' to 'yyyMMdd-HH:mm:ss.SSS'</summary>
/// <param name='field'>The unformatted field</param>
/// <returns>The formatted field</returns>
string toUTCTimestamp (const string& field)
{
	// YYYYMMDDHHMMSSsss -> YYYYMMDD-HH:MM:SS.sss
	unsigned __int64 i = _strtoui64 (field.c_str(), NULL, 10);
	char buf [20];
	//       01234567890123456    01234567 89 01 23 456
	//       YYYYMMDDHHMMSSsss -> YYYYMMDD-HH:MM:SS.sss
	if (i >= 10000000000000000LL)  
	{
		sprintf_s (buf, sizeof(buf), "%017I64d", i);

		string u64 (buf);
		return u64.substr(0, 8) + "-" + u64.substr(8, 2) + ":" + u64.substr (10, 2) + ":" + u64.substr (12, 2) + "." + u64.substr (14);
	}
	else
	//       01234567890123    01234567 89 01 23
	//       YYYYMMDDHHMMSS -> YYYYMMDD-HH:MM:SS
	{
		sprintf_s (buf, sizeof(buf), "%014I64d", i);

		string u64 (buf);
		return u64.substr(0, 8) + "-" + u64.substr(8, 2) + ":" + u64.substr (10, 2) + ":" + u64.substr (12, 2);
	}
}
/// <summary>Converts a string formatted as 'HHmmss' to 'HH:mm:ss'</summary>
/// <param name='field'>The unformatted field</param>
/// <returns>The formatted field</returns>
string toUTCTimeOnly (const string& field)
{
	// HHMMSS -> HH:MM:SS
	unsigned __int64 i = _strtoui64 (field.c_str(), NULL, 10);
	char buf [20];
	// Note que devemos completar com zeros � esquerda!
	sprintf_s (buf, sizeof(buf), "%06I64d", i);
	string u64 (buf);
	return u64.substr (0, 2) + ":" + u64.substr (2, 2) + ":" + u64.substr (4, 2);
}
/// <summary>Converts a string formatted as 'YYYYMMDD' to 'YYYYMMDD'</summary>
/// <param name='field'>The unformatted field</param>
/// <returns>The formatted field</returns>
string toUTCDateOnly (const string& field)
{
	// YYYYMMDD -> YYYYMMDD
	unsigned __int64 i = _strtoui64 (field.c_str(), NULL, 10);
	char buf [20];
	// Note que devemos completar com zeros � esquerda!
	sprintf_s (buf, sizeof(buf), "%08I64d", i);
	return string (buf);
}

void splitTokenizedConfigEntry(const string &strConfigValue, set<string> &lst )
{
	boost::algorithm::split (lst, strConfigValue, boost::algorithm::is_any_of (";"));
}

bool isMessageInFilter(const string &strMessageType, const set<string> &lst )
{
    return lst.find (strMessageType) != lst.end();
}

std::string getCurrentLocalDateTime()
{
	char sCurrentLocalDateTime [24];
	SYSTEMTIME localST;
	::GetLocalTime (&localST);

	// "12345678901234567890123"
	// "20120213T104804.572835"
	::sprintf_s (sCurrentLocalDateTime, "%04d%02d%02dT%02d%02d%02d.%06d",
        localST.wYear, localST.wMonth, localST.wDay, 
        localST.wHour, localST.wMinute, localST.wSecond, 1000 * localST.wMilliseconds);
	return sCurrentLocalDateTime; 
}

};
