#pragma once

#include "stdafx.h"

#include "TCPReplayer.h"
#include "Util.h"

namespace FIX
{
	std::size_t hash_value(const SessionID & s)
	{
		boost::hash<std::string> hasher;
		return hasher(s.getTargetCompID());
	}
};

namespace UMDF
{
	CTCPReplayer::CTCPReplayer(LPCTSTR szPluginID) : m_strPluginID(szPluginID) , m_uiIDGenSeqNum(0)
	{
	}

	CTCPReplayer::~CTCPReplayer(void)
	{
	}


	bool CTCPReplayer::OnLoad()
	{
		CComVariant vtConfig;
		std::string strConfig;
		
		GW_INFO(_T("[UMDFTCPReplayer] Loading configuration ... "));		

		///<summary>
		/// Get the persist messages flag.
		///</summary>
		try 
		{
			strConfig = "PersistMessages";
			GetPluginConfig(strConfig.c_str(), vtConfig, VT_BOOL);
			
			m_config.bPersistMessages = vtConfig.boolVal;
			GW_INFO_FORMAT(_T("[UMDFTCPREPLAYER] %s = %d"), strConfig.c_str() % (m_config.bPersistMessages == true ? 1 : 0))
		}
		catch(BTS::Common::Win32::CException &e)
		{
			GW_WARNING_FORMAT(_T("%s, assuming default == 0"), e.getMessage() )
		}

		///<summary>
		/// Get the path to persist messages.
		///</summary>
		try 
		{
			strConfig = "MessagesFilePath";
			GetPluginConfig(strConfig.c_str(), vtConfig, VT_BSTR);
			
			m_config.m_strMessagesFilePath = static_cast<_bstr_t>(vtConfig.bstrVal);
			GW_INFO_FORMAT(_T("[UMDFTCPREPLAYER] %s = %s"), strConfig.c_str() % m_config.m_strMessagesFilePath.c_str() )
		}
		catch(BTS::Common::Win32::CException &e)
		{
			GW_WARNING_FORMAT(_T("%s, assuming default == %s"), e.getMessage() % m_config.m_strMessagesFilePath.c_str())
		}
				

		///<summary>
		/// Get the max chunk size to encode the new fix messages.
		///</summary>
		try 
		{
			strConfig = "MaxChunkSize";
			GetPluginConfig(strConfig.c_str(), vtConfig, VT_I4);
			ASSERT( vtConfig.uintVal > 0 );
			if ( vtConfig.uintVal > 0 )
				m_config.iMaxChunkSize = vtConfig.uintVal;
			GW_INFO_FORMAT(_T("[UMDFTCPREPLAYER] %s = %d"), strConfig.c_str() % m_config.iMaxChunkSize)
		}
		catch(BTS::Common::Win32::CException &e)
		{
			GW_WARNING_FORMAT(_T("%s, assuming default == %d"), e.getMessage() % m_config.iMaxChunkSize)
		}

		///<summary>
		/// Get the Channels.
		///</summary>
		try 
		{
			strConfig = "Channels";
			GetPluginConfig(strConfig.c_str(), vtConfig, VT_BSTR);
			std::string strChannels = static_cast<_bstr_t>(vtConfig.bstrVal);

			m_config.ParseChannels( strChannels );
			GW_INFO_FORMAT(_T("[UMDFTCPREPLAYER] %s = %s"), strConfig.c_str() % strChannels.c_str() )
		}
		catch(BTS::Common::Win32::CException &e)
		{
			GW_WARNING_FORMAT(_T("%s, assuming default == ''"), e.getMessage() )
		}

		
		///<summary>
		/// Get the circular buffer size.
		///</summary>
		try 
		{
			strConfig = "MemCircularBufferSize";
			GetPluginConfig(strConfig.c_str(), vtConfig, VT_I4);
			ASSERT( vtConfig.uintVal > 0 );
			if ( vtConfig.uintVal > 0 )
				m_config.m_iCircularBufferSize = vtConfig.uintVal;
			GW_INFO_FORMAT(_T("[UMDFTCPREPLAYER] %s = %d"), strConfig.c_str() % m_config.m_iCircularBufferSize)
		}
		catch(BTS::Common::Win32::CException &e)
		{
			GW_WARNING_FORMAT(_T("%s, assuming default == %d"), e.getMessage() % m_config.m_iCircularBufferSize)
		}

		///<summary>
		/// Get the maximum limit of messages per resend request.
		///</summary>
		try 
		{
			strConfig = "MaximumQueryableSize";
			GetPluginConfig(strConfig.c_str(), vtConfig, VT_I4);
			ASSERT( vtConfig.uintVal > 0 );
			if ( vtConfig.uintVal > 0 )
				m_config.m_iMaximumQueryableSize = vtConfig.uintVal;
			GW_INFO_FORMAT(_T("[UMDFTCPREPLAYER] %s = %d"), strConfig.c_str() % m_config.m_iMaximumQueryableSize)
		}
		catch(BTS::Common::Win32::CException &e)
		{
			GW_WARNING_FORMAT(_T("%s, assuming default == %d"), e.getMessage() % m_config.m_iMaximumQueryableSize)
		}

		///<summary>
		/// Get the maximum length of URDR message.
		///</summary>
		try 
		{
			strConfig = "DesiredRawDataLength";
			GetPluginConfig(strConfig.c_str(), vtConfig, VT_I4);
			ASSERT( vtConfig.uintVal > 0 );
			if ( vtConfig.uintVal > 0 )
				m_config.m_iMaxRawDataLength = vtConfig.uintVal;
			GW_INFO_FORMAT(_T("[UMDFTCPREPLAYER] %s = %d"), strConfig.c_str() % m_config.m_iMaxRawDataLength)
		}
		catch(BTS::Common::Win32::CException &e)
		{
			GW_WARNING_FORMAT(_T("%s, assuming default == %d"), e.getMessage() % m_config.m_iMaxRawDataLength)
		}

		bool debugChannels = false;
		///<summary>
		/// Get the persist messages flag.
		///</summary>
		try 
		{
			strConfig = "DebugChannels";
			GetPluginConfig(strConfig.c_str(), vtConfig, VT_BOOL);
			
			debugChannels = vtConfig.boolVal;
			GW_INFO_FORMAT(_T("[UMDFTCPREPLAYER] %s = %d"), strConfig.c_str() % (debugChannels == true ? 1 : 0))
		}
		catch(BTS::Common::Win32::CException &e)
		{
			GW_WARNING_FORMAT(_T("%s, assuming default == 0"), e.getMessage() )
		}


		m_channelManagerPtr.reset( new ChannelManager(m_config) );
		m_channelManagerPtr->InitCircularBuffers();
		
		if ( debugChannels )
		{
			m_channelManagerPtr->InitDebugStoreFiles();			
		}			

		m_ApplMsgReqThreadPtr.reset ( new boost::thread( RequestExec<CProcessApplMsgRequest>( m_ApplMsgReqQMngr ) ) );
		m_WriterThreadPtr.reset( new boost::thread( RequestExec<CWriter> (m_WriterQMngr) ) );

		return true;
	}

	bool CTCPReplayer::OnStart()
	{
		return true;
	}

	void CTCPReplayer::OnStop()
	{
		// m_ApplMsgReqThreadPtr : TODO !
		// m_WriterThreadPtr : TODO !
	}

	void CTCPReplayer::OnLogon(const FIX::SessionID &session)
	{
	}

	void CTCPReplayer::OnLogout(const FIX::SessionID &session)
	{
	}

	UINT CTCPReplayer::OnMessage(const FIX::Message &message, const BTS::Sender &sender)
	{
		FIX::MsgType msgType;		
		message.getHeader().getField(msgType);
		const BTS::SessionInfo * const pSessionInfo = boost::get<BTS::SessionInfo>(&sender);
		const FIX::SessionID & sessionID = pSessionInfo->getSessionID();

		GW_DEBUG_FORMAT(_T("[IN] %s"), message.toString().c_str() );

		if ( pSessionInfo && 
			message.getHeader().getField(FIX::FIELD::BeginString) == FIX::BeginString_FIX44 && 
			msgType.getString() == FIX::MsgType_APPLICATION_MESSAGE_REQUEST
		)
		{			
			
			FIX::ApplReqID fldReqID;
			message.getField( fldReqID );
			FIX::ApplRespID fldRespID(getNextRespID(sessionID));
			const int iApplsCount = message.groupCount( FIX::FIELD::NoApplIDs );

			if ( iApplsCount != 1 )
			{
				FIX44::ApplicationMessageRequestAck reject;
				reject.set(fldRespID);
				reject.set(fldReqID);
				reject.set(FIX::ApplReqType(0));
				reject.set(FIX::ApplRespType(FIX::ApplRespType_EXCEEDED_THE_MAXIMUM_NUMBER_OF_APPLICATIONS));
				m_pGatewayApp->SendToTarget(reject, sessionID );
				return 1;
			}

			FIX44::ApplicationMessageRequest::NoApplIDs noApplIDGroup;
			FIX::ApplBegSeqNum	beginSeqNum;
			std::string			strBeginSeqNum;
			FIX::ApplEndSeqNum	endSeqNum;
			std::string			strEndSeqNum;
			FIX::RefApplID		refApplID;

			long lBegin, lEnd;

			message.getGroup(1, noApplIDGroup);
			if ( noApplIDGroup.isSetField( beginSeqNum ) && noApplIDGroup.isSetField( endSeqNum ) && noApplIDGroup.isSetField( refApplID ) )
			{
				noApplIDGroup.getField( beginSeqNum );
				noApplIDGroup.getField( endSeqNum );			
				noApplIDGroup.getField( refApplID );

				strBeginSeqNum = beginSeqNum.getString();
				strEndSeqNum = endSeqNum.getString();			
				
				if ( IsSessionOngoing( sessionID )   == false)
				{
					ProcessApplMsgRequestPtr processApplMsgRequestPtr( 
						new CProcessApplMsgRequest( this,
													sessionID,
													fldReqID.getString() , 
													fldRespID.getString() , 
													refApplID.getString() , 
													strBeginSeqNum , 
													strEndSeqNum ,
													m_config,
													m_channelManagerPtr) );

					m_ApplMsgReqQMngr.enqueue ( processApplMsgRequestPtr );

					GW_DEBUG_FORMAT(_T("[UMDFTCPREPLAYER] Fix Application Message Request enqueued for session [%s] "), sessionID.toString().c_str() );
				} 				
				else 
				{
					FIX44::ApplicationMessageRequestAck requestAck;
					requestAck.set(FIX::ApplRespID(fldRespID.getString()));
					requestAck.set(FIX::ApplReqID(fldReqID.getString()));
					requestAck.set(FIX::ApplReqType(0));

					FIX44::ApplicationMessageReport appMsgReport;
					appMsgReport.set(FIX::ApplReportID( getNextRptID( sessionID ) )); 					
					appMsgReport.set(FIX::ApplReqID(fldReqID.getString()));
					appMsgReport.set(FIX::ApplReqType(0));
					appMsgReport.set(FIX::ApplRespID(fldRespID.getString()));

					FIX44::ApplicationMessageReport::NoApplIDs		appMsgRepNoApplIDsGroup;
					FIX44::ApplicationMessageRequestAck::NoApplIDs	appGroupReqAck;
					FIX::ApplRespError								fldRespError;
					FIX::ApplRespType								respType;		

					UMDFUtil::DecNumber decBegin = UMDFUtil::DecNumber ( strBeginSeqNum );
					UMDFUtil::DecNumber decEnd = UMDFUtil::DecNumber ( strEndSeqNum );				
					
					appGroupReqAck.set(FIX::RefApplID(refApplID.getString()));			
					appGroupReqAck.set(FIX::ApplBegSeqNum( decBegin ));
					appGroupReqAck.set(FIX::ApplEndSeqNum( decEnd ));

					respType.setValue(FIX::ApplRespType_PRIOR_APPLICATION_REQUEST_IN_PROGRESS);
					requestAck.set( respType );

					requestAck.addGroup( appGroupReqAck );
					reply(requestAck, sessionID );

				}
				
			}
		}

		return 1;
	}

	bool CTCPReplayer::IsSessionOngoing(const FIX::SessionID & sessionID)
	{
		boost::mutex::scoped_lock lock(m_SessionMutex);

		ItMapSessionOngoing itSesOngoing = m_MapSessionOngoing.find( sessionID.toString() );

		if ( itSesOngoing != m_MapSessionOngoing.end() )
		{
			return m_MapSessionOngoing[sessionID.toString()];
		} else {
			return false;
		}
		
	}

	void CTCPReplayer::SetSessionOngoing( const FIX::SessionID & sessionID , bool value )
	{
		boost::mutex::scoped_lock lock(m_SessionMutex);

		m_MapSessionOngoing[ sessionID.toString() ] = value;
	}


	bool CTCPReplayer::store(const std::string& strChannel, FIX::SEQNUM msgSeqNum, const std::string& buffer)
	{
		int iLastEnqueuedSeqNum = 0;

		if ( strChannel.empty() || !m_channelManagerPtr.get() || buffer.length() < 1 )
			return false;
		

		boost::shared_ptr<Channel> channelPtr;
		if ( m_channelManagerPtr->FindChannel( strChannel , channelPtr ) )
		{
			iLastEnqueuedSeqNum = channelPtr->getLastEnqueuedSeqNum();

			///<summary>
			/// at beginning sequence number is zero : keep record.
			///</summary>
			if ( iLastEnqueuedSeqNum == 0 )
			{
				ChannelRecord record(msgSeqNum, buffer );
				channelPtr->put( record );

				GW_INFO_FORMAT(_T("[RECOVERY] CHN %s|SEQ %d|LEN %d"), strChannel.c_str() % msgSeqNum % buffer.length() );
			}
			///<summary>
			/// normal case with correct sequencing : keep record.
			///</summary>
			else if ( iLastEnqueuedSeqNum > 0 && msgSeqNum ==  ( iLastEnqueuedSeqNum + 1 ) )
			{
				ChannelRecord record(msgSeqNum, buffer );
				channelPtr->put( record );

				GW_INFO_FORMAT(_T("[RECOVERY] CHN %s|SEQ %d|LEN %d "), strChannel.c_str() % msgSeqNum % buffer.length() );
			}
			///<summary>
			/// Fix Adapter was reseted , so sequence number is 1 : reset memory and keep record.
			///</summary>
			else if ( msgSeqNum == 1 ) 
			{				
				GW_INFO_FORMAT(_T("=== Channel %s : MEMORY CONTENT ERASED. === ") , strChannel.c_str() );
				channelPtr->reset();

				ChannelRecord record(msgSeqNum, buffer );
				channelPtr->put( record );			
			} 
			///<summary>
			/// error at sequencing : reset memory and keep record.
			///</summary>
			else 
			{
				GW_ERROR_FORMAT(_T("[ STORE ] Message %d at Channel %s is out of sequence ! "), msgSeqNum % strChannel.c_str() );
				
				channelPtr->reset();
				GW_INFO_FORMAT(_T("=== Channel %s : MEMORY CONTENT ERASED. === ") , strChannel.c_str() );
				ChannelRecord record(msgSeqNum, buffer );
				channelPtr->put( record );
			}
		}
	}

	bool CTCPReplayer::resetSeqNum(const std::string& strChannel)
	{

		return true;
	}


	///<summary>
	/// { Interface ITCPReplayer
	///</summary>
	void CTCPReplayer::reply(FIX44::Message &message, const FIX::SessionID & session)
	{
		//GW_DEBUG_FORMAT(_T("[OUT] FIX MESSAGE : %s"), message.toString() );
		
		m_pGatewayApp->SendToTarget( message, session );
		
	}

	std::string CTCPReplayer::getNextRptID(const FIX::SessionID &session)
	{
		return genUID(session,'P');
	}
	std::string CTCPReplayer::getNextRespID(const FIX::SessionID &session)
	{
		return genUID(session,'X');
	}
	///<summary>
	/// } Interface ITCPReplayer
	///</summary>

	std::string CTCPReplayer::genUID(const FIX::SessionID &session, const char ch  )
	{
		std::size_t uiRetVal = boost::hash<FIX::SessionID>()(session);

		uiRetVal += reinterpret_cast<std::size_t>(&session);
		uiRetVal += ::InterlockedIncrement(&m_uiIDGenSeqNum);
		char szRetVal[80];

		sprintf_s (szRetVal, sizeof(szRetVal), "BVMFR%c%010u", ch, uiRetVal); 
		return szRetVal;
	
	}
}