# UMDFTCPRecovery

## UMDFTCPRecovery is a plugin used by the C++ FIX Gateway, a windows server.
  This plugin was used in conjunction with the UMDFMarketDataBroker, who was 
  responsible to broadcast UDP multicast market data. Just in case the broker 
  looses any market data packet, it could ask it to the UMDFTCPRecovery plugin.
  UMDFTCPRecovery would send the lost packets using TCP instead UDP.


