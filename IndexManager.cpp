#include "StdAfx.h"
#include "IndexManager.h"

IndexManager::IndexManager(void)
{
	m_vectMsgIndex.reserve(256);
}

IndexManager::~IndexManager(void)
{
}

void IndexManager::addMessage(__int64 offset, __int64 size, __int64 messageseq)
{
	__int64 i64Index = appendIndexMessage(offset, size);
	m_mapMsgSeqToPos.insert(MapI64::value_type(messageseq, i64Index));
}

BOOL IndexManager::removeMessage(__int64 messageseq)
{
	MapI64::iterator itFindSeqMsg;

	itFindSeqMsg = m_mapMsgSeqToPos.find(messageseq);
	if(itFindSeqMsg != m_mapMsgSeqToPos.end())
	{
		if(itFindSeqMsg->second < (DWORD)m_vectMsgIndex.size())
		{
			m_mapMsgSeqToPos.erase(itFindSeqMsg);

			__int64 i64Index = itFindSeqMsg->second;
			setIndexMessage(i64Index, FileIndex(INVALID_INDEX_VALUE, INVALID_INDEX_VALUE));
			return(TRUE);
		}
		else
		{
			assert(false); // Posi��o Inv�lida?
		}
	}
	return(TRUE);
}

BOOL IndexManager::findMessage(__int64 messageseq, FileIndex &fileindex)
{
	MapI64::iterator itFindSeqMsg;

	itFindSeqMsg = m_mapMsgSeqToPos.find(messageseq);
	if(itFindSeqMsg != m_mapMsgSeqToPos.end())
	{
		if(itFindSeqMsg->second < (DWORD)m_vectMsgIndex.size())
		{
			fileindex = m_vectMsgIndex[static_cast<DWORD>(itFindSeqMsg->second)];

			if(fileindex.isValid())
				return(TRUE);
		}
		else
		{
			assert(false);
		}
	}

	return(FALSE);
}


size_t IndexManager::getIndexCount()
{
	return(m_vectMsgIndex.size());
}

const FileIndex& IndexManager::getIndex(DWORD index)
{
	assert(index < (DWORD)m_vectMsgIndex.size());
	return(m_vectMsgIndex[index]);
}

__int64 IndexManager::appendIndexMessage(__int64 offset, __int64 size)
{
	return appendIndexMessage(FileIndex(offset, size));
}

__int64 IndexManager::appendIndexMessage(const FileIndex &fileindex)
{
	__int64 i64Index = (DWORD)m_vectMsgIndex.size();
	m_vectMsgIndex.push_back(fileindex);
	return(i64Index);
}

void IndexManager::setIndexMessage(__int64 index, __int64 offset,  __int64 size)
{
	setIndexMessage(index, FileIndex(offset,size));
}

void IndexManager::setIndexMessage(__int64 index, const FileIndex &fileindex)
{
	assert(index < (DWORD)m_vectMsgIndex.size());
	m_vectMsgIndex[static_cast<DWORD>(index)] = fileindex;
}