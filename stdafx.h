// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

// Modify the following defines if you have to target a platform prior to the ones specified below.
// Refer to MSDN for the latest info on corresponding values for different platforms.
#ifndef WINVER				// Allow use of features specific to Windows XP or later.
#define WINVER 0x0501		// Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.                   
#define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#endif						

#ifndef _WIN32_WINDOWS		// Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0410 // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE			// Allow use of features specific to IE 6.0 or later.
#define _WIN32_IE 0x0600	// Change this to the appropriate value to target other versions of IE.
#endif

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>



// TODO: reference additional headers your program requires here

// TODO: reference additional headers your program requires here

#include "FIXGateway\SDK/Interfaces/GatewayPlugin.h"
#include "FIXGateway\SDK/Interfaces/IEncodedMessageStorage.h"

//#include "Win32/ReaderWriterLock.h"

#include <deque>
#include <vector>
#include <algorithm>
#include <comdef.h>
#include <stdio.h>

#include <boost/shared_array.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>
#include <boost/thread/locks.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp> 
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/unordered_map.hpp>
#include <boost/circular_buffer.hpp>

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index/mem_fun.hpp>
#include <boost/functional/hash.hpp>

#include "quickfix/values.h"
#include "quickfix/fix44/message.h"
#include "quickfix/fix44/ApplicationMessageRequest.h"
#include "quickfix/fix44/ApplicationMessageRequestAck.h"
#include "quickfix/fix44/ApplicationRawDataReporting.h"
#include "quickfix/fix44/ApplicationMessageReport.h"
#include "quickfix/fix50sp2/MarketDataIncrementalRefresh.h"

using namespace std;
using namespace UMDF;

#include "ChannelRecord.h"
#include "FileIndex.h"
#include "IndexManager.h"
#include "UMDFTCPReplayFileCtrl.h"

#include "Config.h"
#include "Channel.h"
#include "QueueManager.hpp"
#include "RequestExec.hpp"

#include "ProcessApplMsgRequest.h"
#include "Writer.h"
#include "ITCPReplayer.h"




