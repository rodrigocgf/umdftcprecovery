#pragma once

#include "StdAfx.h"

#define BUFFER_READ_SIZE	1024*10 // 10KB -> read buffer 
#define FIELD_I64_SIZE		19


typedef struct _UMDF_TCP_REPLAY_MESSAGE_	UMDF_TCP_REPLAY_MESSAGE;
typedef UMDF_TCP_REPLAY_MESSAGE				*LPUMDF_TCP_REPLAY_MESSAGE;
typedef vector<ChannelRecord>				TCPReplayMsgVect;

struct _UMDF_TCP_REPLAY_MESSAGE_
{
	__int64	Size;
	__int64	SeqMessage;
	string	Message;
};

enum TCP_REPLAY_MSG_LIST_STATUS
{
	NONE		= 0,
	COMPLETE,
	PARCIAL
};

class UMDFTCPReplayFileCtrl
{
	public:
		UMDFTCPReplayFileCtrl(void);
		~UMDFTCPReplayFileCtrl(void);

		void StartUMDFTCPReplayFile(const string& tcpreplayfile,  bool validateStarted = true , bool debug = false);
		__int64 LoadUMDFTCPReplayFile(TCPReplayMsgVect &msglist);
		VOID ClearFile();
		INT Write(UMDF_TCP_REPLAY_MESSAGE &tcpreplaymsg);
		void ResetStarted() { m_bstarted = false; }
		void CloseFile() { _close(m_file); }

		TCP_REPLAY_MSG_LIST_STATUS Read(__int64 startseq, __int64 endseq, TCPReplayMsgVect &msglist);
		TCP_REPLAY_MSG_LIST_STATUS Read(__int64 endseq, TCPReplayMsgVect &msglist);
		TCP_REPLAY_MSG_LIST_STATUS Read(__int64 seq, UMDF_TCP_REPLAY_MESSAGE &msglist);
		

		
	private:
		int				m_file;
		IndexManager	m_indexmanger;
		BOOL			m_bstarted;
		BOOL			m_bloaded;
		__int64			m_i64offsetcontrol;
};
