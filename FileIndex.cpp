#include "StdAfx.h"
#include "FileIndex.h"

	
FileIndex::FileIndex(__int64 offset, __int64 size)
{
	m_i64Offset	= offset;
	m_i64Size	= size;
}

FileIndex::FileIndex(const FileIndex &fileindex)
{
	m_i64Offset	= fileindex.m_i64Offset;
	m_i64Size	= fileindex.m_i64Size;
}

FileIndex::~FileIndex(void)
{
}

BOOL FileIndex::isValid(void)
{
	return ((m_i64Offset != INVALID_INDEX_VALUE) && (m_i64Size != INVALID_INDEX_VALUE));
}