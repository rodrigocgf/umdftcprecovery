#include "StdAfx.h"
#include "Channel.h"

namespace UMDF
{

	Channel::Channel(CConfig config) : m_config(config), m_iCircularBufferSize(0)
	{
	}

	Channel::~Channel(void)
	{
	}

	void Channel::init(int iCircBufferSize , std::string channelID )
	{
		m_strChannelID = channelID;
		m_iCircularBufferSize = iCircBufferSize;				
		m_data.set_capacity( m_iCircularBufferSize + 1 );		
	}

	void Channel::debugInitStoreFile(std::string strChannelID )
	{		
		if ( boost::trim_copy(strChannelID).size() == 0 )
			return;
		
		std::string strFileName = m_config.m_strMessagesFilePath
					+ "\\channel_" + strChannelID + "_messages.dat";
		
		m_fileCtrl.StartUMDFTCPReplayFile(strFileName, true);

		GW_INFO_FORMAT(_T("Loading DAT file into MEMORY : %s\r\n"), strFileName );

		TCPReplayMsgVect tcpReplayMsgs;
		m_fileCtrl.LoadUMDFTCPReplayFile(tcpReplayMsgs);			
		
		vector<ChannelRecord>::iterator itTCPReplayMsgVect;
		boost::circular_buffer<ChannelRecord>::iterator itDataIterator;
		
		itDataIterator = m_data.begin();
		int numRecords = m_config.m_iCircularBufferSize;
		__int64 iLast, iFirst;
		
		itTCPReplayMsgVect = tcpReplayMsgs.end();
		itTCPReplayMsgVect--;

		iLast = (*itTCPReplayMsgVect).getNum();
		for ( ; ( itTCPReplayMsgVect != tcpReplayMsgs.begin() ) && ( numRecords > 0 ); itTCPReplayMsgVect-- )
		{				
			ChannelRecord chRecord(*itTCPReplayMsgVect);
			m_data.push_front(chRecord);
			numRecords--;
		}
		iFirst = (*itTCPReplayMsgVect).getNum() + 1;

		GW_INFO_FORMAT(_T("Channel %s messages at MEMORY : %d - %d\r\n"), strChannelID %  iFirst % iLast);		
		 
	}

	__int64	Channel::getLastEnqueuedSeqNum() 
	{
		if ( m_data.size() > 0 )
			return m_data.back().getNum(); 
		else
			return 0;
	}

	void Channel::reset()
	{
		boost::mutex::scoped_lock lock(m_MutexChannel);

		m_data.clear();	
		GW_ERROR_FORMAT(_T("[TCPREPLAYER] <===  RESETING CHANNEL  %s ===> ") , m_strChannelID );
	}

	void Channel::put(ChannelRecord &r)
	{		
		boost::mutex::scoped_lock lock(m_MutexChannel);

		m_data.push_back( r );
	}

	///<summary>
	/// Assumption that data input is always in a perfect sequence
	/// otherwise a set is required which is more expensive to use
	///
	/// PURPOSE : Recover a sequence of messages stored in memory 
	///
	/// CASES :
	///							    ( A )           ( B )				( C )				( D )				( E )			( F )
	///
	///																						- iEndSeqNum		- iEndSeqNum	- iEndSeqNum
	///
	///
	///
	///																						- iBeginSeqNum
	///
	///			------------- .......................................................................................................................
	///			|			|
	///			|			|	- iEndSeqNum																	- iBeginSeqNum
	///			|   MEMORY	|											- iEndSeqNum
	///			|	WINDOW	|	- iBeginSeqNum
	///			|			|
	///			------------- .......................................................................................................................
	///
	///			 			 						- iEndSeqNum
	///			 			 				        	 											
	///
	///
	///			 			 						- iBeginSeqNum		- iBeginSeqNum											- iBeginSeqNum
	///			 			 
	///			-------------  first sequence number not necessarily 1
	///
	///</summary>
	ResultType Channel::get(int iBeginSeqNum, int iEndSeqNum, std::vector<ChannelRecord> & result , const FIX::SessionID &	sessionID) const
	{		
		boost::mutex::scoped_lock lock(m_MutexChannel);

		boost::circular_buffer<ChannelRecord>::const_iterator itFirst, itLast;
		std::size_t resultSize;	

		if ( (iBeginSeqNum > iEndSeqNum) || 
			((iBeginSeqNum == 0) && (iEndSeqNum == 0)) ||
			(iBeginSeqNum < 0) ||
			(iEndSeqNum < 0 )
			) 
		{ 
			return RtInvalidRange;
		}		

		result.clear();

		ResultType retVal = RtNotFound;	

		if ( iBeginSeqNum >= m_data.front() && iEndSeqNum <= m_data.back() ) // ( A ) 
		{
			result.reserve( iEndSeqNum - iBeginSeqNum + 1 ) ;
			itFirst = std::lower_bound( m_data.begin(), m_data.end(), iBeginSeqNum );		
			itLast = std::upper_bound( m_data.begin() , m_data.end() , iEndSeqNum );
			std::copy( itFirst, itLast, std::back_inserter( result ) );

			retVal = RtOK;

			GW_TRACE_FORMAT(_T("[%s][OUT] MESSAGES NUMBERS FROM %d TO %d"), sessionID.toString().c_str() % iBeginSeqNum % iEndSeqNum );
		}		
		else if ( (iBeginSeqNum < m_data.front()) && (iEndSeqNum >= m_data.front())  && (iEndSeqNum <= m_data.back()) ) // ( C )
		{			

			result.reserve( iEndSeqNum - m_data.front() + 1 );

			itFirst = std::lower_bound( m_data.begin(), m_data.end(), iBeginSeqNum );		
			itLast = std::upper_bound( m_data.begin() , m_data.end() , iEndSeqNum );
			
			std::copy( itFirst, itLast, std::back_inserter( result ) );
		
			retVal = RtBottonNotAvailable;
		}
		else if ( iBeginSeqNum >= m_data.front() && iBeginSeqNum <= m_data.back() && iEndSeqNum > m_data.back() ) // ( E )
		{ 
			result.reserve( m_data.back() - iBeginSeqNum + 1 ) ;
			itFirst = std::lower_bound( m_data.begin(), m_data.end(), iBeginSeqNum );		
			itLast = std::upper_bound( m_data.begin() , m_data.end() , iEndSeqNum );
			std::copy( itFirst, itLast, std::back_inserter( result ) );
			
			retVal = RtTopNotAvailable;

		}
		else if ( (iBeginSeqNum < m_data.front()) && (iEndSeqNum > m_data.back())  ) // ( F )
		{
			result.reserve( m_data.back() - m_data.front() + 1 );			

			itFirst = std::lower_bound( m_data.begin(), m_data.end(), iBeginSeqNum );		
			itLast = std::upper_bound( m_data.begin() , m_data.end() , iEndSeqNum );
			
			std::copy( itFirst, itLast, std::back_inserter( result ) );

			retVal = RtBothTopAndBottonNotAvailable;
		}

		return retVal;
	}
}