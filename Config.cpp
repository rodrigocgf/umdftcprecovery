#include "StdAfx.h"
#include "Config.h"
namespace UMDF
{

	CConfig::CConfig(void) :	iMaxChunkSize(1300), 
								bPersistMessages(false),								
								m_iCircularBufferSize(0),
								m_strMessagesFilePath(""),
								m_iMaximumQueryableSize(2000),
								m_iMaxRawDataLength(1200)
								
	{
	}

	CConfig::~CConfig(void)
	{
	}

	void CConfig::ParseChannels(const string & channels)
	{		
		boost::algorithm::split (m_listChannels, channels, boost::algorithm::is_any_of (";,"));	
		m_listChannels.remove("");
	}

	CConfig::CConfig(const CConfig& other)
	{
		iMaxChunkSize = other.iMaxChunkSize;
		bPersistMessages = other.bPersistMessages;		
		m_iCircularBufferSize = other.m_iCircularBufferSize;
		m_iMaximumQueryableSize = other.m_iMaximumQueryableSize;
		m_iMaxRawDataLength = other.m_iMaxRawDataLength;
		m_strChannel = other.m_strChannel;	
		m_strMessagesFilePath = other.m_strMessagesFilePath;
		m_listChannels = other.m_listChannels;
		
	}

	CConfig& CConfig::operator=(const CConfig& other)
	{
		iMaxChunkSize = other.iMaxChunkSize;
		bPersistMessages = other.bPersistMessages;		
		m_iCircularBufferSize = other.m_iCircularBufferSize;
		m_iMaximumQueryableSize = other.m_iMaximumQueryableSize;
		m_iMaxRawDataLength = other.m_iMaxRawDataLength;
		m_strChannel = other.m_strChannel;		
		m_strMessagesFilePath = other.m_strMessagesFilePath;
		m_listChannels = other.m_listChannels;

		return *this;
	}
}
