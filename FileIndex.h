#if _MSC_VER > 1000
#pragma once
#endif

#define INVALID_INDEX_VALUE ((__int64)-1)

class FileIndex
{
	public:
		FileIndex(__int64 offset = INVALID_INDEX_VALUE, __int64 size = INVALID_INDEX_VALUE);
		FileIndex(const FileIndex &fileindex);
		
		~FileIndex(void);

		BOOL isValid ();

		FileIndex & operator = (const FileIndex &fileindex)
		{
			m_i64Offset	= fileindex.m_i64Offset;
			m_i64Size	= fileindex.m_i64Size;
			return (*this);
		}

		__int64 getOffset(){return m_i64Offset;}
		__int64 getSize(){return m_i64Size;}

	private:
		__int64 m_i64Offset;
		__int64 m_i64Size;
};

typedef std::vector<FileIndex>			FileIndexVector;