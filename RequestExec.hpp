#pragma once

#include "stdafx.h"

namespace UMDF
{
	template<typename T>
	class RequestExec
	{
	public:
		RequestExec(QueueManager<T> & mngr)
		:m_queueMngr(mngr)
		{
		}
		void operator()() 
		{ 
			m_queueMngr();	
		}
	private:
		QueueManager<T> &m_queueMngr;
	};
}