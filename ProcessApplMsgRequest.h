#pragma once

#include "stdafx.h"
#include "DecNumber.hpp"
#include "Config.h"
#include "ChannelManager.h"

namespace UMDF
{
	class ITCPReplayer;	

	class CProcessApplMsgRequest
	{
	public:
		CProcessApplMsgRequest(	ITCPReplayer * const pTCPReplayer , 
								const FIX::SessionID &sessionID,
								const std::string &strReqID, 
								const std::string &strRespID ,
								const std::string &strApplID ,
								const std::string &strBeginSeqNum ,
								const std::string &strEndSeqNum ,
								const CConfig &config ,
								boost::shared_ptr<ChannelManager> channelManagerPtr
																) :	m_pTCPReplayer( pTCPReplayer ) , 
																	m_session(sessionID),
																	m_strReqID(strReqID), 
																	m_strRespID(strRespID),
																	m_strApplID(strApplID),
																	m_strBeginSeqNum(strBeginSeqNum),
																	m_strEndSeqNum(strEndSeqNum),
																	m_config(config) ,
																	m_channelManagerPtr(channelManagerPtr)
		{			
		}

		~CProcessApplMsgRequest(void) {}

		void operator()();
	private:
		CConfig						m_config;
		FIX::SessionID				m_session;		
		std::string					m_strApplID;
		std::string					m_strBeginSeqNum;
		std::string					m_strEndSeqNum;
		std::string					m_strReqID;
		std::string					m_strRespID;
		ITCPReplayer *				m_pTCPReplayer;
		boost::shared_ptr<ChannelManager>	m_channelManagerPtr;				
		int							m_iMaxRawDataLength;
	};

	typedef QueueManager<CProcessApplMsgRequest>::DataTypePtr ProcessApplMsgRequestPtr;

}