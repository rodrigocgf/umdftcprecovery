#pragma once

#include "stdafx.h"
#include "UMDFTCPReplayFileCtrl.h"

namespace UMDF
{
	typedef enum
	{
		RtNotFound = 1,
		RtInvalidRange,
		RtExceededMaxLitmit,
		RtOK,
		RtTopNotAvailable,
		RtBottonNotAvailable,
		RtBothTopAndBottonNotAvailable		
	} ResultType;

	class Channel
	{
	public:
		Channel(CConfig config);
		~Channel(void);

		void		init(int iCircBufferSize, std::string channelID);
		void		debugInitStoreFile(std::string strChannelID );
		void		put(ChannelRecord &r);	
		ResultType	get(int iBeginSeqNum, int iEndSeqNum, std::vector<ChannelRecord> & result , const FIX::SessionID &	sessionID) const;
		void		reset();
		__int64		getLastEnqueuedSeqNum();

	private:
		int											m_iCircularBufferSize;
		boost::circular_buffer<ChannelRecord>		m_data;
		CConfig										m_config;				
		mutable boost::mutex						m_MutexChannel;
		UMDFTCPReplayFileCtrl						m_fileCtrl;
		std::string									m_strChannelID;
	};

}