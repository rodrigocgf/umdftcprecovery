#pragma once

#include <string>
// #include <vcclr.h>

namespace Util
{

enum Encoding
{
    E_ANSI,
    E_UTF8,
    E_BASE64,
    E_UNICODE,
    E_UTF16 = E_UNICODE
};

/// <summary>Converts a string formatted as 'yyyyMMddHHmmss' or 'yyyyMMddHHmmssSSS' to 'yyyMMdd-HH:mm:ss.SSS'</summary>
/// <param name='field'>The unformatted field</param>
/// <returns>The formatted field</returns>
std::string toUTCTimestamp (const std::string& field);
/// <summary>Converts a string formatted as 'HHmmss' to 'HH:mm:ss'</summary>
/// <param name='field'>The unformatted field</param>
/// <returns>The formatted field</returns>
std::string toUTCTimeOnly (const std::string& field);
/// <summary>Converts a string formatted as 'YYYYMMDD' to 'YYYYMMDD'</summary>
/// <param name='field'>The unformatted field</param>
/// <returns>The formatted field</returns>
std::string toUTCDateOnly (const std::string& field);
/// <summary>Converts a string separated by ";" into a set of strings.</summary>
void splitTokenizedConfigEntry(const std::string &strConfigValue, std::set<std::string> &lst );
/// <summary>Checks if something is inside a set of strings.</summary>
bool isMessageInFilter(const std::string &strMessageType, const std::set<std::string> &lst );
/// <summary>Gets the current local date and time as a string like "20120213T104804.572835" </summary>
std::string getCurrentLocalDateTime();
};
