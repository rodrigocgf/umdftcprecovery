#pragma once

#include "stdafx.h"
#include "Channel.h"

namespace UMDF
{
	class ITCPReplayer;
	class IDBChannel;

	class CWriter
	{
	public:
		CWriter(ITCPReplayer * pTCPReplayer , ChannelRecord & record , boost::shared_ptr<Channel> channelPtr ) :		m_pTCPReplayer( pTCPReplayer ) , 	
																														m_Record(record),
																														m_channelPtr(channelPtr) 
		{}

		CWriter(ITCPReplayer * pTCPReplayer , int iNum, const std::string &strData , boost::shared_ptr<Channel> channelPtr ) :		m_pTCPReplayer( pTCPReplayer ) ,																																	
																																	m_channelPtr(channelPtr) ,
																																	m_Record( iNum, strData )
		{}

		~CWriter(void) {}

		void operator()();
	private:		
		ITCPReplayer *							m_pTCPReplayer;
		ChannelRecord 							m_Record;
		boost::shared_ptr<Channel>				m_channelPtr;
	};

	typedef QueueManager<CWriter>::DataTypePtr	WriterPtr;

}
