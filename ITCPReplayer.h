#pragma once 

#include "stdafx.h"

namespace UMDF
{
	class ITCPReplayer
	{
	public:
		virtual void reply(FIX44::Message &messageToSend, const FIX::SessionID & session) = 0;		
		virtual std::string getNextRptID(const FIX::SessionID &session) = 0;
		virtual std::string getNextRespID(const FIX::SessionID &session) = 0;
		virtual bool IsSessionOngoing(const FIX::SessionID & sessionID) = 0;
		virtual void SetSessionOngoing( const FIX::SessionID & sessionID, bool value ) = 0;
	};
}