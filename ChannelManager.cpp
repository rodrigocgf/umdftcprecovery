#include "StdAfx.h"
#include "ChannelManager.h"

namespace UMDF
{

	ChannelManager::ChannelManager(CConfig config) : m_config(config)
	{
	}

	ChannelManager::~ChannelManager(void)
	{
	}

	void ChannelManager::InitCircularBuffers()
	{		
		for (list<string>::iterator it = m_config.m_listChannels.begin(); it != m_config.m_listChannels.end() ; it++ )
		{
			boost::shared_ptr<Channel> channelPtr( new Channel(m_config) );
			std::string strchannel = (*it);
			channelPtr->init( m_config.m_iCircularBufferSize , strchannel );
			
			m_channelMap[ strchannel ] = channelPtr;
		}
	}

	void ChannelManager::InitDebugStoreFiles()
	{
		for (list<string>::iterator it = m_config.m_listChannels.begin(); it != m_config.m_listChannels.end() ; it++ )
		{
			std::string strchannel = (*it);
			boost::shared_ptr<Channel> & channelPtr = m_channelMap[ strchannel ];
			channelPtr->debugInitStoreFile( strchannel );
		}
	}

	bool ChannelManager::FindChannel( const std::string & strChannel, boost::shared_ptr<Channel> & channel )
	{
		std::map< std::string , boost::shared_ptr<Channel> >::iterator itChannelMap;

		itChannelMap = m_channelMap.find( strChannel );
		if ( itChannelMap != m_channelMap.end() )
		{
			channel = itChannelMap->second;
			return true;
		}

		return false;
	}

}