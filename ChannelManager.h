#pragma once

#include "stdafx.h"
#include "Config.h"
#include "Channel.h"

namespace UMDF
{

	class ChannelManager
	{
	public:
		ChannelManager(CConfig config);
		~ChannelManager(void);

		void InitCircularBuffers();
		void InitDebugStoreFiles();
		bool FindChannel( const std::string & strChannel, boost::shared_ptr<Channel> & channel );

	private:
		CConfig										m_config;

		//boost::circular_buffer<DBChannelRecord>		m_data;
		std::map< std::string , boost::shared_ptr<Channel> >			m_channelMap;


	};


}