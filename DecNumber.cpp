#include "stdafx.h"

#include "decNumber.hpp"
extern "C" {
#include <decimal128.h>
};
#include <string>

//-----------------------------------------------------------------------------
namespace UMDFUtil 
//-----------------------------------------------------------------------------
{
using std::string;

//-----------------------------------------------------------------------------
DecContext::DecContext () 
{
    ::decContextDefault (this, DEC_INIT_BASE);
    this->traps = 0;
    this->digits = 34;
}
//-----------------------------------------------------------------------------
DecContext& DecContext::clearStatus (uint32_t status) 
{ 
    return *(DecContext*)::decContextClearStatus (this, status); 
}
//-----------------------------------------------------------------------------
DecContext& DecContext::setDefault (int32_t kind) 
{ 
    return *(DecContext*)::decContextDefault(this, kind); 
}
//-----------------------------------------------------------------------------
enum rounding DecContext::getRounding () 
{ 
    return ::decContextGetRounding (this); 
}
//-----------------------------------------------------------------------------
uint32_t DecContext::getStatus() 
{
    return ::decContextGetStatus (this); 
}
//-----------------------------------------------------------------------------
DecContext& DecContext::restoreStatus(uint32_t status, uint32_t mask) 
{ 
    return *(DecContext*)::decContextRestoreStatus(this, status, mask); 
}
//-----------------------------------------------------------------------------
uint32_t DecContext::saveStatus(uint32_t mask) 
{ 
    return ::decContextSaveStatus(this, mask); 
}
//-----------------------------------------------------------------------------
DecContext& DecContext::setRounding (enum rounding rounding) 
{ 
    return *(DecContext*)::decContextSetRounding(this, rounding); 
}
//-----------------------------------------------------------------------------
DecContext& DecContext::setStatus (uint32_t status) 
{ 
    return *(DecContext*)::decContextSetStatus (this, status); 
}
//-----------------------------------------------------------------------------
DecContext& DecContext::setStatusFromString (const string& str) 
{ 
    return *(DecContext*)::decContextSetStatusFromString (this, str.c_str()); 
}
//-----------------------------------------------------------------------------
DecContext& DecContext::setStatusFromStringQuiet (const string& str) 
{ 
    return *(DecContext*)::decContextSetStatusFromStringQuiet (this, str.c_str()); 
}
//-----------------------------------------------------------------------------
DecContext& DecContext::setStatusQuiet (uint32_t status) 
{
    return *(DecContext*)::decContextSetStatusQuiet(this, status); 
}
//-----------------------------------------------------------------------------
string DecContext::statusToString () 
{ 
    return string (::decContextStatusToString (this)); 
}
//-----------------------------------------------------------------------------
int32_t DecContext::testEndian (uint8_t quiet) 
{ 
    return ::decContextTestEndian(quiet); 
}
//-----------------------------------------------------------------------------
uint32_t DecContext::testSavedStatus (uint32_t status, uint32_t mask) 
{ 
    return ::decContextTestSavedStatus(status, mask); 
}
//-----------------------------------------------------------------------------
uint32_t DecContext::testStatus (uint32_t mask) 
{ 
    return ::decContextTestStatus(this, mask); 
}
//-----------------------------------------------------------------------------
DecContext& DecContext::zeroStatus () 
{ 
    return *(DecContext*)::decContextZeroStatus(this); 
}
//-----------------------------------------------------------------------------
DecNumber::DecNumber() 
{ 
    ::decQuadZero (&value_);
}
//-----------------------------------------------------------------------------
DecNumber::DecNumber(const DecNumber& that) 
{
    ::decQuadCopy (&value_, &that.value_);
}
//-----------------------------------------------------------------------------
DecNumber& DecNumber::operator = (const DecNumber& rhs)
{
    ::decQuadCopy (&value_, &rhs.value_);
    return *this;
}
//-----------------------------------------------------------------------------
DecNumber::DecNumber (int32_t i) 
{ 
    ::decQuadFromInt32 (&value_, i); 
}
//-----------------------------------------------------------------------------
DecNumber::DecNumber (uint32_t u) 
{ 
    ::decQuadFromUInt32 (&value_, u); 
}
//-----------------------------------------------------------------------------
DecNumber::DecNumber (const string& str) 
{ 
    DecContext context; 
    ::decQuadFromString (&value_, str.c_str(), &context); 
}
//-----------------------------------------------------------------------------
DecNumber::DecNumber (const string& str, DecContext& context) 
{ 
    ::decQuadFromString (&value_, str.c_str(), &context); 
}
//-----------------------------------------------------------------------------
DecNumber::operator int() 
{ 
    DecContext context;
    return toInt32 (context, DEC_ROUND_DOWN);
}
//-----------------------------------------------------------------------------
//string DecNumber::toString() 
//{ 
//    char buffer[1024]; 
//    return string (::decQuadToString (&value_, buffer)); 
//}
//-----------------------------------------------------------------------------
string DecNumber::toStringFixed() const
{ 
    char buffer[1024]; 
    return string (::decQuadToStringFixed (&value_, buffer)); 
}
//-----------------------------------------------------------------------------
string DecNumber::toEngString() const
{ 
    char buffer[1024]; 
    return string (::decQuadToEngString (&value_, buffer)); 
} 
//-----------------------------------------------------------------------------
uint32_t DecNumber::toUInt32 (DecContext& context, enum rounding rounding) 
{ 
    return ::decQuadToUInt32 (&value_, &context, rounding); 
}
//-----------------------------------------------------------------------------
int32_t DecNumber::toInt32 (DecContext& context, enum rounding rounding) 
{ 
    return ::decQuadToInt32 (&value_, &context, rounding); 
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::abs (DecContext& context) const
{ 
    DecNumber dc;
    ::decQuadAbs (&dc.value_, &value_, &context); 
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::abs (const DecNumber& number, DecContext& context)
{
    DecNumber dc;
    ::decQuadAbs (&dc.value_, &number.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::add (const DecNumber& rhs, DecContext& context) const
{
    DecNumber dc;
    ::decQuadAdd(&dc.value_, &value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::add (const DecNumber& lhs, const DecNumber& rhs, DecContext& context)
{
    DecNumber dc;
    ::decQuadAdd (&dc.value_, &lhs.value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::and (const DecNumber& rhs, DecContext& context) const
{
    DecNumber dc;
    ::decQuadAnd(&dc.value_, &value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::and (const DecNumber& lhs, const DecNumber& rhs, DecContext& context)
{
    DecNumber dc;
    ::decQuadAnd (&dc.value_, &lhs.value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::compare (const DecNumber& rhs, DecContext& context) const
{
    DecNumber dc;
    ::decQuadCompare(&dc.value_, &value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::compare (const DecNumber& lhs, const DecNumber& rhs, DecContext& context)
{
    DecNumber dc;
    ::decQuadCompare (&dc.value_, &lhs.value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
int DecNumber::intCompare (const DecNumber& rhs, DecContext& context) const 
{
    return intCompare (*this, rhs, context);
}
//-----------------------------------------------------------------------------
/*static*/int DecNumber::intCompare (const DecNumber& lhs, const DecNumber& rhs, DecContext& context) 
{ 
    return (int) DecNumber::compare (lhs, rhs, context); 
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::compareSignal (const DecNumber& rhs, DecContext& context) const
{
    DecNumber dc;
    ::decQuadCompareSignal(&dc.value_, &value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::compareSignal (const DecNumber& lhs, const DecNumber& rhs, DecContext& context)
{
    DecNumber dc;
    ::decQuadCompareSignal (&dc.value_, &lhs.value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::compareTotal (const DecNumber& rhs) const
{
    DecNumber dc;
    ::decQuadCompareTotal (&dc.value_, &value_, &rhs.value_);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::compareTotal (const DecNumber& lhs, const DecNumber& rhs)
{
    DecNumber dc;
    ::decQuadCompareTotal (&dc.value_, &lhs.value_, &rhs.value_);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::compareTotalMag (const DecNumber& rhs) const
{
    DecNumber dc;
    ::decQuadCompareTotalMag (&dc.value_, &value_, &rhs.value_);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::compareTotalMag (const DecNumber& lhs, const DecNumber& rhs)
{
    DecNumber dc;
    ::decQuadCompareTotalMag (&dc.value_, &lhs.value_, &rhs.value_);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::divide (const DecNumber& rhs, DecContext& context) const
{
    DecNumber dc;
    ::decQuadDivide (&dc.value_, &value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/ DecNumber DecNumber::divide (const DecNumber& lhs, const DecNumber& rhs, DecContext& context)
{
    DecNumber dc;
    ::decQuadDivide (&dc.value_, &lhs.value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::divideInteger (const DecNumber& rhs, DecContext& context) const
{
    DecNumber dc;
    ::decQuadDivideInteger (&dc.value_, &value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::divideInteger (const DecNumber& lhs, const DecNumber& rhs, DecContext& context)
{
    DecNumber dc;
    ::decQuadDivideInteger (&dc.value_, &lhs.value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::exp (DecContext& context) const
{
    DecNumber dc;
    ::decNumber dn;
    decQuadToNumber (&value_, &dn);
    ::decNumberExp (&dn, &dn, &context);
    decQuadFromNumber (&dc.value_, &dn, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::exp (const DecNumber& number, DecContext& context)
{
    DecNumber dc;
    ::decNumber dn;
    decQuadToNumber (&number.value_, &dn);
    ::decNumberExp (&dn, &dn, &context);
    decQuadFromNumber (&dc.value_, &dn, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::FMA (const DecNumber& rhs, const DecNumber& fhs, DecContext& context) const
{
    DecNumber dc;
    ::decQuadFMA (&dc.value_, &value_, &rhs.value_, &fhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::FMA (const DecNumber& lhs, const DecNumber& rhs, const DecNumber& fhs, DecContext& context)
{
    DecNumber dc;
    ::decQuadFMA (&dc.value_, &lhs.value_, &rhs.value_, &fhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::invert (DecContext& context) const
{
    DecNumber dc;
    ::decQuadInvert (&dc.value_, &value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::invert (const DecNumber& number, DecContext& context)
{
    DecNumber dc;
    ::decQuadInvert (&dc.value_, &number.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::ln (DecContext& context) const
{
    DecNumber dc;
    ::decNumber dn;
    decQuadToNumber (&value_, &dn);
    ::decNumberLn (&dn, &dn, &context);
    decQuadFromNumber (&dc.value_, &dn, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::ln (const DecNumber& number, DecContext& context)
{
    DecNumber dc;
    ::decNumber dn;
    decQuadToNumber (&number.value_, &dn);
    ::decNumberLn (&dn, &dn, &context);
    decQuadFromNumber (&dc.value_, &dn, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::logB (DecContext& context) const
{
    DecNumber dc;
    ::decNumber dn;
    decQuadToNumber (&value_, &dn);
    ::decNumberLogB (&dn, &dn, &context);
    decQuadFromNumber (&dc.value_, &dn, &context);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::logB (const DecNumber& number, DecContext& context)
{
    DecNumber dc;
    ::decNumber dn;
    decQuadToNumber (&number.value_, &dn);
    ::decNumberLogB (&dn, &dn, &context);
    decQuadFromNumber (&dc.value_, &dn, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::log10 (DecContext& context) const
{
    DecNumber dc;
    ::decNumber dn;
    decQuadToNumber (&value_, &dn);
    ::decNumberLog10 (&dn, &dn, &context);
    decQuadFromNumber (&dc.value_, &dn, &context);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::log10 (const DecNumber& number, DecContext& context)
{
    DecNumber dc;
    ::decNumber dn;
    decQuadToNumber (&number.value_, &dn);
    ::decNumberLog10 (&dn, &dn, &context);
    decQuadFromNumber (&dc.value_, &dn, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::max_ (const DecNumber& rhs, DecContext& context) const
{
    DecNumber dc;
    ::decQuadMax (&dc.value_, &value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::max_ (const DecNumber& lhs, const DecNumber& rhs, DecContext& context)
{
    DecNumber dc;
    ::decQuadMax (&dc.value_, &lhs.value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::maxMag (const DecNumber& rhs, DecContext& context) const
{
    DecNumber dc;
    ::decQuadMaxMag (&dc.value_, &value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::maxMag (const DecNumber& lhs, const DecNumber& rhs, DecContext& context)
{
    DecNumber dc;
    ::decQuadMaxMag (&dc.value_, &lhs.value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::min_ (const DecNumber& rhs, DecContext& context) const
{
    DecNumber dc;
    ::decQuadMin (&dc.value_, &value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::min_ (const DecNumber& lhs, const DecNumber& rhs, DecContext& context)
{
    DecNumber dc;
    ::decQuadMax (&dc.value_, &lhs.value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::minMag (const DecNumber& rhs, DecContext& context) const
{
    DecNumber dc;
    ::decQuadMinMag (&dc.value_, &value_, &rhs.value_, &context);
    return dc;}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::minMag (const DecNumber& lhs, const DecNumber& rhs, DecContext& context)
{
    DecNumber dc;
    ::decQuadMinMag (&dc.value_, &lhs.value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::minus (DecContext& context) const
{
    DecNumber dc;
    ::decQuadMinus (&dc.value_, &value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::minus (const DecNumber& number, DecContext& context)
{
    DecNumber dc;
    ::decQuadMinus (&dc.value_, &number.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::multiply (const DecNumber& rhs, DecContext& context) const
{
    DecNumber dc;
    ::decQuadMultiply (&dc.value_, &value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::multiply (const DecNumber& lhs, const DecNumber& rhs, DecContext& context)
{
    DecNumber dc;
    ::decQuadMultiply (&dc.value_, &lhs.value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::or (const DecNumber& rhs, DecContext& context) const
{
    DecNumber dc;
    ::decQuadOr (&dc.value_, &value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::or (const DecNumber& lhs, const DecNumber& rhs, DecContext& context)
{
    DecNumber dc;
    ::decQuadOr (&dc.value_, &lhs.value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::plus (DecContext& context) const
{
    DecNumber dc;
    ::decQuadPlus (&dc.value_, &value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::plus (const DecNumber& number, DecContext& context)
{
    DecNumber dc;
    ::decQuadPlus (&dc.value_, &number.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::power (const DecNumber& rhs, DecContext& context) const
{
    DecNumber dc;
    ::decNumber dnLhs, dnRhs;
    decQuadToNumber (&value_, &dnLhs);
    decQuadToNumber (&rhs.value_, &dnRhs);
    ::decNumberPower (&dnLhs, &dnLhs, &dnRhs, &context);
    decQuadFromNumber (&dc.value_, &dnLhs, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::power (const DecNumber& lhs, const DecNumber& rhs, DecContext& context)
{
    DecNumber dc;
    ::decNumber dnLhs, dnRhs;
    decQuadToNumber (&lhs.value_, &dnLhs);
    decQuadToNumber (&rhs.value_, &dnRhs);
    ::decNumberPower (&dnLhs, &dnLhs, &dnRhs, &context);
    decQuadFromNumber (&dc.value_, &dnLhs, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::quantize (const DecNumber& rhs, DecContext& context) const
{
    DecNumber dc;
    ::decQuadQuantize (&dc.value_, &value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::quantize (const DecNumber& lhs, const DecNumber& rhs, DecContext& context)
{
    DecNumber dc;
    ::decQuadQuantize (&dc.value_, &lhs.value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::reduce (DecContext& context) const
{
    DecNumber dc;
    ::decQuadReduce (&dc.value_, &value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::reduce (const DecNumber& number, DecContext& context)
{
    DecNumber dc;
    ::decQuadReduce (&dc.value_, &number.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::remainder (const DecNumber& rhs, DecContext& context) const
{
    DecNumber dc;
    ::decQuadRemainder (&dc.value_, &value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::remainder (const DecNumber& lhs, const DecNumber& rhs, DecContext& context)
{
    DecNumber dc;
    ::decQuadRemainder (&dc.value_, &lhs.value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::remainderNear (const DecNumber& rhs, DecContext& context) const
{
    DecNumber dc;
    ::decQuadRemainderNear (&dc.value_, &value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::remainderNear (const DecNumber& lhs, const DecNumber& rhs, DecContext& context)
{
    DecNumber dc;
    ::decQuadRemainderNear (&dc.value_, &lhs.value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::rotate (const DecNumber& rhs, DecContext& context) const
{
    DecNumber dc;
    ::decQuadRotate (&dc.value_, &value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::rotate (const DecNumber& lhs, const DecNumber& rhs, DecContext& context)
{
    DecNumber dc;
    ::decQuadRotate (&dc.value_, &lhs.value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
bool DecNumber::sameQuantum (const DecNumber& rhs) const
{
    return ::decQuadSameQuantum (&value_, &rhs.value_) != 0;
}
//-----------------------------------------------------------------------------
/*static*/bool DecNumber::sameQuantum (const DecNumber& lhs, const DecNumber& rhs)
{
    return ::decQuadSameQuantum (&lhs.value_, &rhs.value_) != 0;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::scaleB (const DecNumber& rhs, DecContext& context) const
{
    DecNumber dc;
    ::decQuadScaleB (&dc.value_, &value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::scaleB (const DecNumber& lhs, const DecNumber& rhs, DecContext& context)
{
    DecNumber dc;
    ::decQuadScaleB (&dc.value_, &lhs.value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::shift (const DecNumber& rhs, DecContext& context) const
{
    DecNumber dc;
    ::decQuadShift (&dc.value_, &value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::shift (const DecNumber& lhs, const DecNumber& rhs, DecContext& context)
{
    DecNumber dc;
    ::decQuadShift (&dc.value_, &lhs.value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::squareRoot (DecContext& context) const
{
    DecNumber dc;
    ::decNumber dn;
    decQuadToNumber (&value_, &dn);
    ::decNumberSquareRoot (&dn, &dn, &context);
    decQuadFromNumber (&dc.value_, &dn, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::squareRoot (const DecNumber& number, DecContext& context)
{
    DecNumber dc;
    ::decNumber dn;
    decQuadToNumber (&number.value_, &dn);
    ::decNumberSquareRoot (&dn, &dn, &context);
    decQuadFromNumber (&dc.value_, &dn, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::subtract (const DecNumber& rhs, DecContext& context) const
{
    DecNumber dc;
    ::decQuadSubtract (&dc.value_, &value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::subtract (const DecNumber& lhs, const DecNumber& rhs, DecContext& context)
{
    DecNumber dc;
    ::decQuadSubtract (&dc.value_, &lhs.value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::toIntegralExact (DecContext& context) const
{
    DecNumber dc;
    ::decQuadToIntegralExact (&dc.value_, &value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::toIntegralExact (const DecNumber& number, DecContext& context)
{
    DecNumber dc;
    ::decQuadToIntegralExact (&dc.value_, &number.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::toIntegralValue (DecContext& context, enum rounding rounding) const
{
    DecNumber dc;
    ::decQuadToIntegralValue (&dc.value_, &value_, &context, rounding);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::toIntegralValue (const DecNumber& number, DecContext& context, enum rounding rounding)
{
    DecNumber dc;
    ::decQuadToIntegralValue (&dc.value_, &number.value_, &context, rounding);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::xor (const DecNumber& rhs, DecContext& context) const
{
    DecNumber dc;
    ::decQuadXor (&dc.value_, &value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::xor (const DecNumber& lhs, const DecNumber& rhs, DecContext& context)
{
    DecNumber dc;
    ::decQuadXor (&dc.value_, &lhs.value_, &rhs.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
enum decClass DecNumber::getClass ()
{
    return ::decQuadClass (&value_);
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::copy (const DecNumber& number)
{
    return DecNumber (number);
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::copyAbs (const DecNumber& number)
{
    DecNumber dc;
    ::decQuadCopyAbs (&dc.value_, &number.value_);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::copyNegate (const DecNumber& number)
{
    DecNumber dc;
    ::decQuadCopyNegate (&dc.value_, &number.value_);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::copySign (const DecNumber& source, const DecNumber& pattern)
{
    DecNumber dc;
    ::decQuadCopySign (&dc.value_, &source.value_, &pattern.value_);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::copySign (const DecNumber& pattern) const
{
    DecNumber dc;
    ::decQuadCopySign (&dc.value_, &value_, &pattern.value_);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::nextMinus (DecContext& context) const
{
    DecNumber dc;
    ::decQuadNextMinus (&dc.value_, &value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::nextMinus (const DecNumber& number, DecContext& context)
{
    DecNumber dc;
    ::decQuadNextMinus (&dc.value_, &number.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::nextPlus (DecContext& context) const
{
    DecNumber dc;
    ::decQuadNextPlus (&dc.value_, &value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
/*static*/DecNumber DecNumber::nextPlus (const DecNumber& number, DecContext& context)
{
    DecNumber dc;
    ::decQuadNextPlus (&dc.value_, &number.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::nextToward (const DecNumber& y, DecContext& context) const
{
    DecNumber dc;
    ::decQuadNextToward (&dc.value_, &value_, &y.value_, &context);
    return dc;
}
//-----------------------------------------------------------------------------
DecNumber DecNumber::nextToward (const DecNumber& x, const DecNumber& y, DecContext& context)
{
    DecNumber dc;
    ::decQuadNextToward (&dc.value_, &x.value_, &y.value_, &context);
    return dc;}
//-----------------------------------------------------------------------------
string DecNumber::version ()
{
    return string (decQuadVersion());
}
//-----------------------------------------------------------------------------
DecNumber& DecNumber::zero()
{
    return *(DecNumber*)::decQuadZero(&value_);
}
//-----------------------------------------------------------------------------
int32_t DecNumber::isNormal() const
{
    return ::decQuadIsNormal (&value_);
}
//-----------------------------------------------------------------------------
int32_t DecNumber::isNormal(DecNumber& number)
{
    return ::decQuadIsNormal (&number.value_);
}
//-----------------------------------------------------------------------------
int32_t DecNumber::isSubnormal() const
{
    return ::decQuadIsSubnormal (&value_);
}
//-----------------------------------------------------------------------------
int32_t DecNumber::isSubnormal(DecNumber& number)
{
    return ::decQuadIsSubnormal (&number.value_);
}
//-----------------------------------------------------------------------------
bool DecNumber::isCanonical() const
{
    return true;
}
//-----------------------------------------------------------------------------
bool DecNumber::isCanonical(DecNumber& number)
{
    return true;
}
//-----------------------------------------------------------------------------
bool DecNumber::isFinite() const
{
    return decQuadIsFinite (&value_) != 0;
}
//-----------------------------------------------------------------------------
bool DecNumber::isFinite(DecNumber& number)
{
    return decQuadIsFinite (&number.value_) != 0;
}
//-----------------------------------------------------------------------------
bool DecNumber::isInfinite() const
{
    return decQuadIsInfinite (&value_) != 0;
}
//-----------------------------------------------------------------------------
bool DecNumber::isInfinite(DecNumber& number)
{
    return decQuadIsInfinite (&number.value_) != 0;
}
//-----------------------------------------------------------------------------
bool DecNumber::isNaN() const
{
    return decQuadIsNaN (&value_) != 0;
}
//-----------------------------------------------------------------------------
bool DecNumber::isNaN(DecNumber& number)
{
    return decQuadIsNaN (&number.value_) != 0;
}
//-----------------------------------------------------------------------------
bool DecNumber::isNegative() const
{
    return decQuadIsSigned (&value_) != 0;
}
//-----------------------------------------------------------------------------
bool DecNumber::isNegative(DecNumber& number)
{
    return decQuadIsSigned (&number.value_) != 0;
}
//-----------------------------------------------------------------------------
bool DecNumber::isSpecial() const
{
    return !isFinite();
}
//-----------------------------------------------------------------------------
bool DecNumber::isSpecial(DecNumber& number)
{
    return !isFinite (number);
}
//-----------------------------------------------------------------------------
bool DecNumber::isZero() const
{
    return decQuadIsZero (&value_) != 0;
}
//-----------------------------------------------------------------------------
bool DecNumber::isZero(DecNumber& number)
{
    return decQuadIsZero (&number.value_) != 0;
}
//-----------------------------------------------------------------------------
int DecNumber::radix()
{
    return 10;
}
//-----------------------------------------------------------------------------
};
//-----------------------------------------------------------------------------

