#include "StdAfx.h"
#include "ProcessApplMsgRequest.h"

namespace UMDF
{

	void CProcessApplMsgRequest::operator()()
	{
		m_pTCPReplayer->SetSessionOngoing( m_session  , true);

		FIX44::ApplicationMessageRequestAck requestAck;
		requestAck.set(FIX::ApplRespID(m_strRespID));
		requestAck.set(FIX::ApplReqID(m_strReqID));
		requestAck.set(FIX::ApplReqType(0));

		FIX44::ApplicationMessageReport appMsgReport;
		appMsgReport.set(FIX::ApplReportID( m_pTCPReplayer->getNextRptID( m_session ) ));		
		appMsgReport.set(FIX::ApplReqID(m_strReqID));
		appMsgReport.set(FIX::ApplReqType(0));
		appMsgReport.set(FIX::ApplRespID(m_strRespID));

		FIX44::ApplicationMessageReport::NoApplIDs		appMsgRepNoApplIDsGroup;
		FIX44::ApplicationMessageRequestAck::NoApplIDs	appGroupReqAck;
		FIX::ApplRespError								fldRespError;
		FIX::ApplRespType								respType;		

		UMDFUtil::DecNumber decBegin = UMDFUtil::DecNumber ( m_strBeginSeqNum );
		UMDFUtil::DecNumber decEnd = UMDFUtil::DecNumber ( m_strEndSeqNum );				
		
		appGroupReqAck.set(FIX::RefApplID(m_strApplID));			
		appGroupReqAck.set(FIX::ApplBegSeqNum( decBegin ));
		appGroupReqAck.set(FIX::ApplEndSeqNum( decEnd ));

		boost::shared_ptr<Channel>	channelPtr;
		if ( ! m_channelManagerPtr->FindChannel( m_strApplID , channelPtr ) )
		{		
			fldRespError.setValue(FIX::ApplRespError_APPLICATION_DOES_NOT_EXIST);			
			appGroupReqAck.set(fldRespError);

			respType.setValue(FIX::ApplRespType_APPLICATION_DOES_NOT_EXIST);
			requestAck.set( respType );

			requestAck.addGroup( appGroupReqAck );
			m_pTCPReplayer->reply(requestAck, m_session );

			m_pTCPReplayer->SetSessionOngoing( m_session , false );
			return;
		}		

		if ( decBegin.isNaN() || decEnd.isNaN() || (decBegin <= 0) || (decEnd <= 0) )
		{
			fldRespError.setValue(FIX::ApplRespError_INVALID_RANGE_REQUESTED);
			appGroupReqAck.set(fldRespError);
			respType.setValue(FIX::ApplRespType_APPLICATION_REQUEST_NOT_ACCEPTED_OR_PARTIALLY_ACCEPTED);
			requestAck.set( respType );
			requestAck.addGroup( appGroupReqAck );
			m_pTCPReplayer->reply(requestAck, m_session );

			m_pTCPReplayer->SetSessionOngoing( m_session , false );
			return;
		}

		if ( ( (decEnd - decBegin) >= m_config.m_iMaximumQueryableSize ) && ( m_config.m_iMaximumQueryableSize > 0 ) )
		{	
			respType.setValue(FIX::ApplRespType_APPLICATION_REQUEST_NOT_ACCEPTED_OR_PARTIALLY_ACCEPTED);
			requestAck.set( respType );

			appGroupReqAck.set(FIX::ApplRespError(FIX::ApplRespError_EXCEEDED_MAXIMUM_LIMIT_OF_MESSAGES_ALLOWED));
			requestAck.addGroup( appGroupReqAck );
			m_pTCPReplayer->reply(requestAck, m_session );

			m_pTCPReplayer->SetSessionOngoing( m_session , false );
			return;
		}

		

		if ( decEnd >= decBegin )
		{

			respType.setValue(FIX::ApplRespType_APPLICATION_REQUEST_ACCEPTED);
			requestAck.set( respType );
			requestAck.addGroup( appGroupReqAck );
			m_pTCPReplayer->reply(requestAck, m_session );
			
			appMsgRepNoApplIDsGroup.set(FIX::RefApplID( m_strApplID ));

			std::vector<ChannelRecord> result;
			ResultType retVal = channelPtr->get(decBegin, decEnd , result, m_session);
			if ( ( retVal >= RtOK ) )
			{	
				
				FIX44::ApplicationRawDataReporting appURDR;
				FIX44::ApplicationRawDataReporting::NoApplSeqNums groupNoApplSeqNum;
				
				appURDR.set(FIX::ApplReqID(m_strReqID));
				appURDR.set(FIX::ApplRespID(m_strRespID));
				appURDR.set(FIX::ApplID(m_strApplID));
				appURDR.set(FIX::ApplResendFlag(true));
				appURDR.set(FIX::TotNumReports(result.size()));

				
				std::string strRawDataOutput;
				FIX::FieldBase fieldBase;

				__int64 iLastSeqNum = 0;
				__int64 iTotalLength = 0;
				BOOST_FOREACH(ChannelRecord & record, result)
				{
					if ( iTotalLength >= m_config.m_iMaxRawDataLength )
					{
						//fieldBase.setString( strRawDataOutput );
						fieldBase.setValue( strRawDataOutput.c_str() , strRawDataOutput.length() );
						FIX::RawData rawData(fieldBase);
						appURDR.set( rawData );
						//appURDR.set(FIX::RawData( fieldBase ) );
						appURDR.set(FIX::RawDataLength(iTotalLength));
						m_pTCPReplayer->reply( appURDR, m_session );
						
						iTotalLength = 0;
						appURDR.removeGroup( groupNoApplSeqNum );						
						strRawDataOutput.clear();
					}

					groupNoApplSeqNum.set( FIX::ApplLastSeqNum( iLastSeqNum ) );
					groupNoApplSeqNum.set( FIX::ApplSeqNum( record.getNum() ) );
					groupNoApplSeqNum.set( FIX::RawDataLength( record.getLength() ) );
					groupNoApplSeqNum.set( FIX::RawDataOffset( iTotalLength ) );
					appURDR.addGroup( groupNoApplSeqNum );
					
					iLastSeqNum = record.getNum();
					iTotalLength += record.getLength();
					
					strRawDataOutput.append( record.getData(), record.getLength() );					
				}

				//fieldBase.setString( strRawDataOutput );
				fieldBase.setValue( strRawDataOutput.c_str() , strRawDataOutput.length() );
				FIX::RawData rawData(fieldBase);
				appURDR.set( rawData );

				//appURDR.set(FIX::RawData( fieldBase ) );
				

				appURDR.set(FIX::RawDataLength(iTotalLength));
				m_pTCPReplayer->reply( appURDR, m_session );				

				if ( retVal > RtOK ) 
				{
					respType.setValue(FIX::ApplRespType_APPLICATION_REQUEST_NOT_ACCEPTED_OR_PARTIALLY_ACCEPTED);

					
					if ( retVal == RtBottonNotAvailable )
						appMsgRepNoApplIDsGroup.set(FIX::ApplRespError(FIX::ApplRespError_PENDING_BOTTOM_N_MESSAGES));
					else if ( retVal == RtTopNotAvailable )
						appMsgRepNoApplIDsGroup.set(FIX::ApplRespError(FIX::ApplRespError_TOP_N_MESSAGES_NOT_AVAILABLE));
					else if ( retVal == RtBothTopAndBottonNotAvailable )
						appMsgRepNoApplIDsGroup.set(FIX::ApplRespError(FIX::ApplRespError_BOTH_TOP_N_NOT_AVAILABLE_AND_PENDING_BOTTOM_N));						
				}				
				
				appMsgRepNoApplIDsGroup.set(FIX::RefApplLastSeqNum(result.back().getNum()));

				if ( retVal > RtOK ) 
				{
					if ( retVal == RtBottonNotAvailable )
						appMsgRepNoApplIDsGroup.set(FIX::ApplRespError(FIX::ApplRespError_PENDING_BOTTOM_N_MESSAGES));
					else if ( retVal == RtTopNotAvailable )
						appMsgRepNoApplIDsGroup.set(FIX::ApplRespError(FIX::ApplRespError_TOP_N_MESSAGES_NOT_AVAILABLE));
					else if ( retVal == RtBothTopAndBottonNotAvailable )
						appMsgRepNoApplIDsGroup.set(FIX::ApplRespError(FIX::ApplRespError_BOTH_TOP_N_NOT_AVAILABLE_AND_PENDING_BOTTOM_N));						

					appMsgReport.set(FIX::ApplReportType(FIX::ApplReportType_APPLICATION_MESSAGE_RESEND_ERROR));
				} else 
					appMsgReport.set(FIX::ApplReportType(FIX::ApplReportType_APPLICATION_MESSAGE_RESEND_COMPLETED));		

			} else {
				appMsgReport.set(FIX::ApplReportType(FIX::ApplReportType_APPLICATION_MESSAGE_RESEND_ERROR));
				appMsgRepNoApplIDsGroup.set(FIX::ApplRespError(FIX::ApplRespError_MESSAGES_REQUESTED_ARE_NOT_AVAILABLE));
			}

			appMsgReport.addGroup( appMsgRepNoApplIDsGroup );
			m_pTCPReplayer->reply( appMsgReport, m_session );

		} else {

			fldRespError.setValue(FIX::ApplRespError_INVALID_RANGE_REQUESTED);			
			appGroupReqAck.set(fldRespError);

			respType.setValue(FIX::ApplRespType_APPLICATION_REQUEST_NOT_ACCEPTED_OR_PARTIALLY_ACCEPTED);
			requestAck.set( respType );

			requestAck.addGroup( appGroupReqAck );
			m_pTCPReplayer->reply(requestAck, m_session );
			
		}

		m_pTCPReplayer->SetSessionOngoing( m_session , false );

	}

}