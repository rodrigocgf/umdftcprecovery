#include "StdAfx.h"
#include "UMDFTCPReplayFileCtrl.h"

#include <boost/filesystem.hpp>

#define MBYTE (1024*1024)

UMDFTCPReplayFileCtrl::UMDFTCPReplayFileCtrl(void)
{
	m_bstarted			= FALSE;
	m_bloaded			= FALSE;
	m_file				= -1;
	m_i64offsetcontrol	= 0;
}

VOID UMDFTCPReplayFileCtrl::ClearFile()
{
	if(m_bstarted)
		_close(m_file);

	StartUMDFTCPReplayFile("", false);
}

UMDFTCPReplayFileCtrl::~UMDFTCPReplayFileCtrl(void)
{
	if(m_bstarted)
		_close(m_file);
}

/// <summary>
///
/// Quando for poss�vel fazer o load do arquivo, devemos utilizar a flag _O_APPEND, hoje
/// utilizamos a flag _O_TRUNC para limpar o arquivo, para n�o corrermos o risco de termos sequenciais repetidos,
/// j� que a engine do UMDF zera o sequencial quando reinicia.
///
/// </summary>
void UMDFTCPReplayFileCtrl::StartUMDFTCPReplayFile(const string& tcpreplayfile,  bool validateStarted , bool debug)
{
	if(validateStarted)
	{
		if(m_bstarted)
		{
			return;
		}
	}
	
	boost::filesystem::path pathTcpReplayFile       (tcpreplayfile);
	
	
	m_file = _sopen(tcpreplayfile.c_str(),  _O_BINARY | _O_RANDOM | _O_RDWR, _SH_DENYWR, _S_IREAD | _S_IWRITE);

	if(-1 == m_file) 
	{
		GW_ERROR_FORMAT(_T("[TCPREPLAYER] Error opening file %s"), tcpreplayfile.c_str() );
		throw std::exception(strerror(errno));
	}
	

	m_bstarted = TRUE;
}

__int64 UMDFTCPReplayFileCtrl::LoadUMDFTCPReplayFile(TCPReplayMsgVect &msglist)
{
	CHAR					*pbuffer=NULL;
	CHAR					*pbufferMsg = NULL;
	CHAR					bufferaux[FIELD_I64_SIZE+1];
	string					bufferread;
	__int64					bytesread;
	__int64					i64pos;
	__int64					i64buffersize;
	__int64					i64msgsize;
	__int64					i64msgseq;
	__int64					i64offset;
	__int64					i64filesize;
	

	
	if(!m_bstarted)
		throw std::exception("UMDFTCPReplayFileCtrl not started.");

	if(-1 == m_file)
		throw std::exception("UMDFTCPReplay file is closed.");

	i64filesize = _lseeki64(m_file, 0, SEEK_END);
	i64buffersize = (i64filesize < (30*MBYTE)) ? i64filesize : (2*MBYTE);

	_lseeki64(m_file, 0, SEEK_SET);
	i64offset = 0;

	pbuffer = new char [static_cast<size_t>(i64buffersize)];
	if(!pbuffer)
		throw std::exception("UMDFTCPReplayFileCtrl error allocating memory.");

	i64offset = 0;
	bytesread = 1;

	while(i64filesize > 0 && bytesread > 0)
	{
		memset(pbuffer, 0, sizeof(pbuffer));		
		bytesread =_read(m_file, pbuffer, static_cast<DWORD>(i64buffersize));
		if(bytesread > 0)
		{
			i64filesize -= bytesread;
			i64pos = 0;

			while((i64pos + FIELD_I64_SIZE) < bytesread)
			{
				memcpy(bufferaux,&pbuffer[i64pos],FIELD_I64_SIZE); 
				bufferaux[FIELD_I64_SIZE]=0;
				i64msgsize = _atoi64(bufferaux);

				if((i64pos + i64msgsize) > bytesread)
				{
					_lseeki64(m_file, i64offset, SEEK_SET);
					break;
				}

				memcpy(bufferaux,&pbuffer[i64pos+FIELD_I64_SIZE],FIELD_I64_SIZE);
				bufferaux[FIELD_I64_SIZE]=0;
				i64msgseq = _atoi64(bufferaux);					

				m_indexmanger.addMessage(i64offset, i64msgsize, i64msgseq);

				pbufferMsg = new char [static_cast<size_t>(i64msgsize)];
				memcpy(pbufferMsg, &pbuffer[i64pos+2*FIELD_I64_SIZE], i64msgsize);

				bufferread.clear();
				bufferread.append( pbufferMsg , i64msgsize );				
				ChannelRecord tcpreplaymsg = ChannelRecord(i64msgseq, bufferread);
				msglist.push_back(tcpreplaymsg);

				delete [] pbufferMsg;

				i64pos+=i64msgsize;
				i64offset+=i64msgsize;
				
			}
		}
	}

	
	delete [] pbuffer;

	return i64msgseq;
}


TCP_REPLAY_MSG_LIST_STATUS UMDFTCPReplayFileCtrl::Read(__int64 startseq, __int64 endseq, TCPReplayMsgVect &msglist)
{
	__int64						i64count;
	int							ipos;
	char						*pbuffer;
	FileIndex					fileindex;
	string						buffer;
	TCP_REPLAY_MSG_LIST_STATUS	retcode = (TCP_REPLAY_MSG_LIST_STATUS)COMPLETE;

	if(0 == m_indexmanger.getIndexCount())
		return((TCP_REPLAY_MSG_LIST_STATUS)NONE);

	i64count = startseq;
	while(i64count <= endseq)
	{
		if(m_indexmanger.findMessage(i64count, fileindex))
		{
			pbuffer = NULL;
			_lseeki64(m_file, fileindex.getOffset(), SEEK_SET);
			pbuffer = new char[static_cast<size_t>(fileindex.getSize())];
			if(pbuffer)
			{
				if(_read(m_file, pbuffer, static_cast<DWORD>(fileindex.getSize())) == fileindex.getSize())
				{			
					buffer.clear();
					buffer.append( pbuffer , fileindex.getSize() );
					ipos = 0;

					__int64 size = _atoi64(buffer.substr(ipos,FIELD_I64_SIZE).c_str());
					ipos += FIELD_I64_SIZE;

					__int64 seqMessage = _atoi64(buffer.substr(ipos,FIELD_I64_SIZE).c_str());
					ipos += FIELD_I64_SIZE;

					std::string message = buffer.substr(ipos, static_cast<size_t>(size - ipos));
															
					ChannelRecord tcpreplaymsg = ChannelRecord(seqMessage, message);
					msglist.push_back(tcpreplaymsg);
				}
				else
					retcode = (TCP_REPLAY_MSG_LIST_STATUS)PARCIAL;

				delete [] pbuffer;
			}
		}
		else
			retcode = (TCP_REPLAY_MSG_LIST_STATUS)NONE;

		i64count++;
	}
	return(retcode);
}

INT UMDFTCPReplayFileCtrl::Write(UMDF_TCP_REPLAY_MESSAGE &tcpreplaymsg)
{
	stringstream	strbuffer;
	int				iret;

	tcpreplaymsg.Size = (FIELD_I64_SIZE + FIELD_I64_SIZE + tcpreplaymsg.Message.length());
	
	strbuffer << setfill('0') << setw(19) << tcpreplaymsg.Size 
			  << setfill('0') << setw(19) << tcpreplaymsg.SeqMessage 
			  << tcpreplaymsg.Message;

	_lseeki64(m_file, m_i64offsetcontrol, SEEK_SET);

	iret = _write(m_file, strbuffer.str().data(), static_cast<DWORD>(strbuffer.str().length()));

	if(-1 == iret)
		throw std::exception(strerror(errno));

	m_indexmanger.addMessage(m_i64offsetcontrol, tcpreplaymsg.Size, tcpreplaymsg.SeqMessage);	

	m_i64offsetcontrol += tcpreplaymsg.Size;

	return(iret);
}

TCP_REPLAY_MSG_LIST_STATUS UMDFTCPReplayFileCtrl::Read(__int64 endseq, TCPReplayMsgVect &msglist)
{
	return(Read(0, endseq, msglist));
}

TCP_REPLAY_MSG_LIST_STATUS UMDFTCPReplayFileCtrl::Read(__int64 seq, UMDF_TCP_REPLAY_MESSAGE &tcpreplaymsg)
{
	int							ipos;
	char						*pbuffer;
	FileIndex					fileindex;
	string						buffer;
	TCP_REPLAY_MSG_LIST_STATUS	retcode = (TCP_REPLAY_MSG_LIST_STATUS)COMPLETE;

	if(0 == m_indexmanger.getIndexCount())
		return((TCP_REPLAY_MSG_LIST_STATUS)NONE);

	if(m_indexmanger.findMessage(seq, fileindex))
	{
		pbuffer = NULL;
		_lseeki64(m_file, fileindex.getOffset(), SEEK_SET);
		pbuffer = new char[static_cast<size_t>(fileindex.getSize())];
		if(pbuffer)
		{
			if(_read(m_file, pbuffer, static_cast<DWORD>(fileindex.getSize())) == fileindex.getSize())
			{
				buffer = pbuffer;
				ipos = 0;

				tcpreplaymsg.Size = _atoi64(buffer.substr(ipos,FIELD_I64_SIZE).c_str());
				ipos += FIELD_I64_SIZE;

				tcpreplaymsg.SeqMessage = _atoi64(buffer.substr(ipos,FIELD_I64_SIZE).c_str());
				ipos += FIELD_I64_SIZE;

				tcpreplaymsg.Message = buffer.substr(ipos, static_cast<size_t>(tcpreplaymsg.Size - ipos)).c_str();
			}
			else
				retcode = (TCP_REPLAY_MSG_LIST_STATUS)NONE;

			delete [] pbuffer;
		}
	}
	else
		retcode = (TCP_REPLAY_MSG_LIST_STATUS)NONE;

	return(retcode);
}

